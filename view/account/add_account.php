<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/qemail/Account/save" 
                  id="formQerana" name="formQerana" method="POST" class="form-horizontal"
                  accept-charset="utf-8">

                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>
                <div class='form-group form-group-sm row small'> 
                    <label for='f_account' class='col-sm-3 col-form-label'>Name</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_account' name='f_account' 

                                   class='form-control form-control-sm' required   />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_address' class='col-sm-3 col-form-label'>Mail Address</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='email' id='f_address' name='f_address' 

                                   class='form-control form-control-sm' required   />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_from_name' class='col-sm-3 col-form-label'>Mail From Name</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_from_name' name='f_from_name' 

                                   class='form-control form-control-sm'    />
                        </div>   
                    </div>   
                </div>   


                <div class="card border">

                    <div class="card-header">
                        Server settings
                    </div>
                    <div class="card-body">
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_username' class='col-sm-3 col-form-label'>Mail Username</label>  
                            <div class='col-sm-9'>  
                                <div class='input-group col-sm-8'>   
                                    <input type='text' id='f_username' name='f_username' 

                                           class='form-control form-control-sm' required   />
                                </div>   
                            </div>   
                        </div>   
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_password' class='col-sm-3 col-form-label'>Mail Password</label>  
                            <div class='col-sm-4'>  
                                <div class='input-group col-sm-8'>   
                                    <input type='password' id='f_password' name='f_password' 

                                           class='form-control form-control-sm' required   />

                                </div>   
                            </div>   
                        </div>   
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_smtp_server' class='col-sm-3 col-form-label'>Mail Smtp Server</label>  
                            <div class='col-sm-9'>  
                                <div class='input-group col-sm-8'>   
                                    <input type='text' id='f_smtp_server' name='f_smtp_server' 

                                           class='form-control form-control-sm' required   />
                                </div>   
                            </div>   
                        </div>   
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_smtp_auth' class='col-sm-3 col-form-label'>Mail Smtp Auth</label>  
                            <div class='col-sm-3'>  
                                <div class='input-group col-sm-8'>   
                                    <select class="form-control form-control-sm" name="f_smtp_auth" id="f_smtp_auth">
                                        <option value="1" selected>True</option>
                                        <option value="0">False</option>
                                    </select>
                                </div>   
                            </div>   
                        </div>   
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_smtp_port' class='col-sm-3 col-form-label'>Mail Smtp Port</label>  
                            <div class='col-sm-3'>  
                                <div class='input-group col-sm-8'>   
                                    <input type='number' id='f_smtp_port' name='f_smtp_port' 
                                           class='form-control form-control-sm' value="587" required   />
                                </div>   
                            </div>   
                        </div>   

                        <div class='form-group form-group-sm row small'> 
                            <label for='f_smtp_secure' class='col-sm-3 col-form-label'>Mail Smtp Secure</label>  
                            <div class='col-sm-4'>  
                                <div class='input-group col-sm-8'>   
                                    <select class="form-control form-control-sm" name="f_smtp_secure" id="f_smtp_secure">
                                        <option value="0">None</option>
                                        <option value="ssl" selected>ssl</option>
                                        <option value="tls">tls</option>
                                        <option value="ssl-tls">ssl-tls</option>
                                    </select>
                                </div>   
                            </div>   
                        </div>   
                    </div>

                </div>



                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>

            </form>
        </div>
    </div>
</div>
<script>

// submit form
//    $('#formQerana').submit(function (e)
//    {
//        e.preventDefault();
//        var form = $(this);
//        var url = form.attr('action');
//
//        $.ajax({
//            type: "POST",
//            url: url,
//            data: form.serialize(), // serializes the form's elements.
//            success: function (data)
//            {
//                $('#modalLg').modal('hide');
//                loadAccount();
//            }
//        });
//
//
//    });
//

</script>


