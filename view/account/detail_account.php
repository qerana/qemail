<div class="container-fluid" >

    <section class="content-header">
        <h6 class="text-black">
            Detalle Account

        </h6>
    </section> 

    <section class='content'>
        <div class="row">
            <div class="col-xl-8 col-lg-7">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between bg-gray-100">
                        <h6 class="m-0 font-weight-bold text-primary"> 
                            <a href="/qemail/Account/index" class="btn btn-info btn-circle" title="volver">
                                <i class="fa fa-arrow-left"></i>
                            </a>
                            <span id="detail_title">  <?php echo $Account->account; ?> </span>
                        </h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle no-gutters" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="text-gray-400"></i>
                                Options
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="top-end" >
                                <a href="#" class="dropdown-item"
                                   data-target="#modalLg" 
                                   data-remote="/qemail/Account/edit/<?php echo $id_account; ?>"
                                   data-toggle="modal"
                                   data-titlemodal='edit Account'
                                   >
                                    <span class="icon">
                                        <i class="fas fa-edit"></i>
                                    </span>
                                    <span class="text">Edit</span>
                                </a>
                                <a href="/qemail/Account/test/<?php echo $id_account; ?>" 
                                   class="dropdown-item"
                                   >
                                    <span class="icon">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                                    <span class="text">Test setting</span>
                                </a>

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="/qemail/Account/delete/<?php echo $id_account; ?>">
                                    <span class="icon">
                                        <i class="fas fa-trash"></i>
                                    </span>
                                    <span class="text">Delete</span>    
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">

                        <div class="container-fluid small" id="data_Account" data-Account="<?php echo $id_account; ?>">
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Email</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_mail_address">
                                    <?php echo $Account->address; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>From name</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_mail_from_name">
                                    <?php echo $Account->from_name; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Username</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_mail_username">
                                    <?php echo $Account->username; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Smtp server</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_mail_smtp_server">
                                    <?php echo $Account->smtp_server; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Smtp auth</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_mail_smtp_auth">
                                    <?php echo $Account->smtp_auth; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Smtp port</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_mail_smtp_port">
                                    <?php echo $Account->smtp_port; ?>
                                </div>
                            </div>

                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Smtp secure</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_mail_smtp_secure">
                                    <?php echo $Account->smtp_secure; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="card-footer">
                        <h6>Tasks</h6>
                       <ul class="list-group list-group-horizontal small">
                            <?php
                            foreach ($Categories AS $Category):

                                $link = 'activateTaskAccount/' . $Account->id_account.'/'.$Category->id_category;

                                if ($Category->id_account == $Account->id_account) {
                                    $class_task = 'list-group-item-success';
                                    $link .= '/0';
                                } else {
                                    $class_task = '';
                                    $link .= '/' . $Account->id_account;
                                }
                                ?>
                                <li class="list-group-item <?php echo $class_task; ?> ">
                                    <a href="/qemail/Account/<?php echo $link; ?>">
                                        <?php echo $Category->category; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">
                            <span class="badge bg-primary text-white"><?php echo $Account->num_emails; ?>

                            </span>
                            Email Sended

                        </h6>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function () {

    });


</script>
