<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/qemail/Account/save" 
                  id="formQerana" name="formQerana" method="POST" class="form-horizontal"
                  accept-charset="utf-8">
                <input type="hidden" name="f_id_account" id="f_id_account" value="<?php echo $Account->id_account; ?>">
                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>
                <div class='form-group form-group-sm row small'> 
                    <label for='f_account' class='col-sm-3 col-form-label'>Account</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_account' name='f_account' 

                                   class='form-control form-control-sm' required   value='<?php echo $Account->account; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_address' class='col-sm-3 col-form-label'>Address</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='email' id='f_address' name='f_address' 

                                   class='form-control form-control-sm' required   value='<?php echo $Account->address; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_from_name' class='col-sm-3 col-form-label'>From Name</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_from_name' name='f_from_name' 

                                   class='form-control form-control-sm'    value='<?php echo $Account->from_name; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_username' class='col-sm-3 col-form-label'>Username</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_username' name='f_username' 

                                   class='form-control form-control-sm' required   value='<?php echo $Account->username; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_password' class='col-sm-3 col-form-label'>Password</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='password' id='f_password' name='f_password' 
                                   autocomplete="off" readonly onfocus="this.removeAttribute('readonly');"
                                   class='form-control form-control-sm' required   
                                   value='<?php echo $Account->get_decrypt_pass(); ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class="card shadow mb-4 bg-gray-100">
                    <div class="card-header text-blue">Server settings</div>
                    <div class="card-body">
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_smtp_server' class='col-sm-3 col-form-label'>Smtp Server</label>  
                            <div class='col-sm-9'>  
                                <div class='input-group col-sm-8'>   
                                    <input type='text' id='f_smtp_server' name='f_smtp_server' 

                                           class='form-control form-control-sm' required   value='<?php echo $Account->smtp_server; ?>' />
                                </div>   
                            </div>   
                        </div>   
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_smtp_auth' class='col-sm-3 col-form-label'>Smtp Auth</label>  
                            <div class='col-sm-4'>  
                                <div class='input-group col-sm-8'>  
                                    
                                    <select name="f_smtp_auth" id="f_smtp_auth" class="form-control form-control-sm">
                                        <option value="1" <?php echo ($Account->smtp_auth == 1) ? 'selected' : '';?>>Yes</option>
                                        <option value="0" <?php echo ($Account->smtp_auth == 0) ? 'selected' : '';?>>No</option>
                                    </select>
                                </div>   
                            </div>   
                        </div>   
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_smtp_port' class='col-sm-3 col-form-label'>Smtp Port</label>  
                            <div class='col-sm-3'>  
                                <div class='input-group col-sm-8'>   
                                    <input type='number' id='f_smtp_port' name='f_smtp_port' 
                                           class='form-control form-control-sm'  required  value='<?php echo $Account->smtp_port; ?>'  />
                                </div>   
                            </div>   
                        </div>   

                        <div class='form-group form-group-sm row small'> 
                            <label for='f_smtp_secure' class='col-sm-3 col-form-label'>Smtp Secure</label>  
                            <div class='col-sm-9'>  
                                <div class='input-group col-sm-8'>   
                                    <input type='text' id='f_smtp_secure' name='f_smtp_secure' 

                                           class='form-control form-control-sm'    value='<?php echo $Account->smtp_secure; ?>' />
                                </div>   
                            </div>   
                        </div>   
                    </div>   
                </div>   

                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>

            </form>
        </div>
    </div>
</div>
<script>

//// submit form
//    $('#formQerana').submit(function (e)
//    {
//        e.preventDefault();
//        var form = $(this);
//        var url = form.attr('action');
//
//        $.ajax({
//            type: "POST",
//            url: url,
//            data: form.serialize(), // serializes the form's elements.
//            success: function (data)
//            {
//                $('#modalLg').modal('hide');
//                loadDataAccount();
//            }
//        });
//
//
//    });


</script>


