<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/qemail/EmailCategory/save" 
                  id="formQerana" name="formQerana" method="POST" class="form-horizontal"
                  accept-charset="utf-8">
                <input type="hidden" name="f_id_category" id="f_id_category" value="<?php echo $EmailCategory->id_category; ?>">
                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>
                <div class='form-group form-group-sm row small'> 
                    <label for='f_category' class='col-sm-3 col-form-label'>Category</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_category' name='f_category' 

                                   class='form-control form-control-sm'    value='<?php echo $EmailCategory->category; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_id_account' class='col-sm-3 col-form-label'>Id Account</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='number' id='f_id_account' name='f_id_account' 
                                   class='form-control form-control-sm'  required  value='<?php echo $EmailCategory->id_account; ?>'  />
                        </div>   
                    </div>   
                </div>   

                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>

            </form>
        </div>
    </div>
</div>
<script>

// submit form
    $('#formQerana').submit(function (e)
    {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data)
            {
                $('#modalLg').modal('hide');
                loadDataEmailCategory();
            }
        });


    });


</script>


