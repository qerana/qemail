<script>
    tinymce.init({
        selector: '#f_tpl_content',
        height: 500,
        theme: 'modern',
        force_p_newlines: false,
        force_br_newlines: true,
        menubar: false,
        statusbar: false,
        forced_root_block: '',
        plugins: 'print preview  searchreplace autolink directionality visualblocks \n\
           visualchars fullscreen image link media template codesample table charmap hr pagebreak \n\
           nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount  \n\
           imagetools  contextmenu colorpicker textpattern help',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | fontselect | fontsizeselect | code',
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
</script>
<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/qemail/Template/modify" 
                  id="formQerana" name="formQerana" method="POST" class="form-horizontal1"
                  accept-charset="utf-8">
                <input type="hidden" name="f_id_qemail_tpl" id="f_id_qemail_tpl" value="<?php echo $Template->id_qemail_tpl; ?>">
                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>
                <div class='form-group form-group-sm row small'> 
                    <label for='f_tpl_name' class='col-sm-3 col-form-label'>Name</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_tpl_name' name='f_tpl_name' 

                                   class='form-control form-control-sm' required   value='<?php echo $Template->tpl_name; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_tpl_type' class='col-sm-3 col-form-label'>Type</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_tpl_type' name='f_tpl_type' 

                                   class='form-control form-control-sm' required   value='<?php echo $Template->tpl_type; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_tpl_content' class='col-sm-3 col-form-label'>Tpl Content</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <textarea id='f_tpl_content' name='f_tpl_content' required
                                      class='form-contro form-control-sml' cols='60' rows='5'><?php echo $Template->tpl_content; ?></textarea>
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_tpl_settings' class='col-sm-3 col-form-label'>Settings</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <textarea id='f_tpl_settings' name='f_tpl_settings' 
                                      class='form-contro form-control-sml' cols='60' rows='5'><?php echo $Template->tpl_settings; ?></textarea>
                        </div>   
                    </div>   
                </div>   

                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>

            </form>
        </div>
    </div>
</div>
<script>

// submit form
    $('#formQerana').submit(function (e)
    {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data)
            {
                $('#modalLg').modal('hide');
            }
        });


    });


</script>


