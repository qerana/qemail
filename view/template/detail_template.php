<div class="container-fluid" >

    <section class="content-header">
        <h6 class="text-black">
            Detalle Template

        </h6>
    </section> 

    <section class='content'>
        <div class="row">
            <div class="col-xl-12 col-lg-7">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between bg-gray-100">
                        <h6 class="m-0 font-weight-bold text-primary"> 
                            <a href="/qemail/Template/index" class="btn btn-info btn-circle" title="volver">
                                <i class="fa fa-arrow-left"></i>
                            </a>
                            <span id="detail_title">  </span>
                        </h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="top-end" >
                                <div class="dropdown-header">Opciones:</div>

                                <a href="#" class="dropdown-item"
                                   data-target="#modalLg" 
                                   data-remote="/qemail/Template/edit/<?php echo $id_qemail_tpl; ?>"
                                   data-toggle="modal"
                                   data-titlemodal='editar Template'
                                   >
                                    <span class="icon">
                                        <i class="fas fa-edit"></i>
                                    </span>
                                    <span class="text">Editar</span>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="/qemail/Template/delete/<?php echo $id_qemail_tpl; ?>">
                                    <span class="icon">
                                        <i class="fas fa-trash"></i>
                                    </span>
                                    <span class="text">Eliminar</span>    
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">

                        <div class="container-fluid small" id="data_Template" data-Template="<?php echo $id_qemail_tpl; ?>">
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Name</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_tpl_name">
                                    <?php echo $Template->tpl_name; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Type</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_tpl_type">
                                    <?php echo $Template->tpl_type; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Content</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_tpl_content">
                                    <?php echo $Template->tpl_content; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Active</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_tpl_active">
                                    <?php echo $Template->tpl_active; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Additional settings</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_tpl_settings">
                                    <?php echo $Template->tpl_settings; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

        </div>

    </section>
</div>
<script>
    $(document).ready(function () {

    });


</script>