
<script>
    tinymce.init({
        selector: '#f_body',
        height: 500,
        theme: 'modern',
        force_p_newlines: false,
        force_br_newlines: true,
        menubar: false,
        statusbar: false,
        forced_root_block: '',
        plugins: 'print preview  searchreplace autolink directionality visualblocks \n\
           visualchars fullscreen image link media template codesample table charmap hr pagebreak \n\
           nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount  \n\
           imagetools  contextmenu colorpicker textpattern help',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | fontselect | fontsizeselect | code',
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
</script>
<div class="modal-body">
    <div class="card shadow mb-4">
        <form action="/qemail/Email/save" 
              id="formNewEmail" name="formNewEmail" method="POST" class="form-horizontal"
              accept-charset="utf-8">
            <div class="card-body">


                <?php echo $kerana_token; ?>
                <div class='form-group form-group-sm row small'> 
                    <label for='f_id_account' class='col-sm-3 col-form-label'>FROM</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <select class="form-control form-control-sm" name="f_id_account" id="f_id_account" required>
                                <option value="">--Select account --</option>
                                <?php foreach ($Accounts AS $Account): ?>
                                    <option value="<?php echo $Account->id_account; ?>">
                                        <?php echo $Account->address; ?>
                                    </option>

                                <?php endforeach; ?>
                            </select>
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_destination' class='col-sm-3 col-form-label'>TO;</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>  

                            <textarea id='f_destination' name='f_destination' required
                                      class='form-contro form-control-sml' cols='60' rows='1'></textarea>
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_bcc' class='col-sm-3 col-form-label'>BCC</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <textarea id='f_bcc' name='f_bcc' 
                                      class='form-contro form-control-sml' cols='60' rows='1'></textarea>
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_subject' class='col-sm-3 col-form-label'>SUBJECT</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_subject' name='f_subject' 

                                   class='form-control form-control-sm' value="test"   />
                        </div>   
                    </div>   
                </div>   
                <header class="breadcrumb">

                    <button type="button" id="btn-save-email" class="btn btn-success btn-sm">OK</button> &nbsp;
                </header>

            </div>
            <div class="card-body">
                <textarea id='f_body' name='f_body' required
                          class='form-control form-control-sml' cols='60' rows='5'></textarea>
            </div>
        </form>
    </div>
</div>
<script>


    $('#btn-save-email').click(function (e) {

        $('#f_body').html(tinymce.get('f_body').getContent());
        //e.preventDefault();
        var form = $('#formNewEmail');
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data)
            {
                $('#modalTiny').modal('hide');
               
            }
        });
    });



</script>


