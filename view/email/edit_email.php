<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/qemail/Email/save" 
                  id="formQerana" name="formQerana" method="POST" class="form-horizontal"
                  accept-charset="utf-8">
                <input type="hidden" name="f_id_email" id="f_id_email" value="<?php echo $Email->id_email; ?>">
                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>
                <div class='form-group form-group-sm row small'> 
                    <label for='f_id_mail_account' class='col-sm-3 col-form-label'>Id Mail Account</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='int' id='f_id_mail_account' name='f_id_mail_account' 
                                   class='form-control form-control-sm'  required  value='<?php echo $Email->id_mail_account; ?>'  />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_destination' class='col-sm-3 col-form-label'>Destination</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <textarea id='f_destination' name='f_destination' required
                                      class='form-contro form-control-sml' cols='60' rows='5'><?php echo $Email->destination; ?></textarea>
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_bcc' class='col-sm-3 col-form-label'>Bcc</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <textarea id='f_bcc' name='f_bcc' 
                                      class='form-contro form-control-sml' cols='60' rows='5'><?php echo $Email->bcc; ?></textarea>
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_subject' class='col-sm-3 col-form-label'>Subject</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_subject' name='f_subject' 

                                   class='form-control form-control-sm'    value='<?php echo $Email->subject; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_body' class='col-sm-3 col-form-label'>Body</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <textarea id='f_body' name='f_body' required
                                      class='form-contro form-control-sml' cols='60' rows='5'><?php echo $Email->body; ?></textarea>
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_created_at' class='col-sm-3 col-form-label'>Created At</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   

                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_created_by' class='col-sm-3 col-form-label'>Created By</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_created_by' name='f_created_by' 

                                   class='form-control form-control-sm'    value='<?php echo $Email->created_by; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_sw_status' class='col-sm-3 col-form-label'>Sw Status</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='number' id='f_sw_status' name='f_sw_status' 
                                   class='form-control form-control-sm'  required  value='<?php echo $Email->sw_status; ?>'  />
                        </div>   
                    </div>   
                </div>   

                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Grabar</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancelar
                    </button>
                </header>

            </form>
        </div>
    </div>
</div>
<script>

// submit form
    $('#formQerana').submit(function (e)
    {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data)
            {
                $('#modalLg').modal('hide');
                loadDataEmail();
            }
        });


    });


</script>


