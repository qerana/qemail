<div class="container-fluid" >

    <section class='content'>
        <div class="row">
            <div class="col-xl-8 col-lg-7">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between bg-gray-100">
                        <h6 class="m-0 font-weight-bold text-primary"> 
                            <a href="/qemail/Email/index" class="btn btn-info btn-circle" title="volver">
                                <i class="fa fa-arrow-left"></i>
                            </a>
                            <span id="detail_title"><?php echo $Email->subject; ?>  </span>
                        </h6>
                        <div class="dropdown no-arrow">
                            <?php echo $Email->created_at; ?>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">

                        <div class="container-fluid small" id="data_Email" data-Email="<?php echo $id_email; ?>">
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>FROM</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_id_mail_account">
                                    <?php echo $Email->Account->from_name . ' (' . $Email->Account->address . ')'; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>TO:</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_destination">
                                    <?php echo $Email->destination; ?>
                                </div>
                            </div>
                            <?php if ($Email->bcc) { ?>
                                <div class="row m-1">
                                    <div class="col-sm-3 bg-gray-100 p-2">
                                        <b>BCC</b>
                                    </div>
                                    <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_bcc">
                                        <?php echo $Email->bcc; ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>SUBJECT</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_subject">
                                    <?php echo $Email->subject; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    Attachments
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_created_by">
                                    <?php foreach ($Email->attachments AS $Attachment): ?>
                                        <li><?php echo $Attachment->path_attachment; ?></li>

                                    <?php endforeach; ?>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <ul class="breadcrumb">
                            <li>
                                <a href="/qemail/email/send/<?php echo $Email->id_email; ?>" class="btn btn-outline-primary" title="send">
                                    <i class="fa fa-envelope"></i>
                                </a>
                            </li>
                            <li>&nbsp;</li>
                            <li>
                                <a href="/qemail/email/delete/<?php echo $Email->id_email; ?>" class="btn btn-outline-warning" title="delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </li>

                        </ul>
                        <?php echo $Email->body; ?>
                    </div>
                </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-gray-900">Logs</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <table class="table table-hover table-condensed table-bordered">
                            <thead class="bg-gray-800 text-white">
                                <tr class="small">
                                    <th>Message</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody class="small">
                                <?php
                                // set logs
                                $Email->setLogs();
                                $Logs = $Email->logs;


                                foreach ($Logs AS $Log):
                                    $class_log = ($Log->message_log == 'Sended!') ? 'text-success' : 'text-danger';
                                    ?>
                                    <tr class="small">
                                        <td class="<?php echo $class_log; ?>"><?php echo $Log->message_log; ?></td>
                                        <td><?php echo $Log->log_timestamp; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function () {

    });


</script>