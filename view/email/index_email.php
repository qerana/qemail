<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=vsnb1rt4r5aaahhfaaay7o499r648y37aqr7kan70j6nqq5o"></script> 

<div class="container-fluid">

    <section class="content-header">
        <h5 class="text-primary">
            <i class="fa fa-envelope fa-2x"></i> Emails
        </h5>
        <ol class="breadcrumb">
            <li>
                <input type="text" class="form-control input-sm" 
                       id="filter_search" 
                       name="filter_search"
                       aria-describedby="inputSuccess3Status"
                       value="" placeholder="Buscador"/>
            </li>
        </ol>

    </section> 


    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold">
                total:<span class="badge" id="total"></span>
            </h6>
            <div class="btn btn-primary">
                <a href="#" class="dropdown-item1 text-white" title="Nuevo email"
                   data-target="#modalTiny" 
                   data-remote="/qemail/Email/add"
                   data-toggle="modal"
                   data-titlemodal=' new Email'
                   >
                    <span class="icon">
                        <i class="fas fa-envelope"></i>
                        <i class="fas fa-plus"></i>
                    </span>
                </a>
            </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">
            <section id='results' class='table-responsive'>
                <div id="loader"></div>
                <table class="table_search table table-bordered table-condensed table-hover">
                    <thead class='bg-gray-600 text-white'>
                        <tr class="small">
                            <td class="bg-gray-800"></td>
                            <td>From</td>
                            <td>TO</td>
                            <td>Subject</td>
                            <td>Sw Status</td>
                            <td>Date</td>


                        </tr>
                    </thead>
                    <tbody class="small" id="list_Email">
                        <!-- via json ajax -->
                    </tbody>
                </table>
            </section>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        loadEmail();



// search html data


    });

    $("#filter_search").keyup(function () {
        var filter = $(this).val();

        $("table.table_search > tbody > tr").each(function () {

            // si la lista no existe el string a mostrar, ocultamos las filas
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();

                // Si coincide mostramos el tr
            } else {
                $(this).show();

            }
        });

    });

</script>