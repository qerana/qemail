<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\controller;

use Qerapp\qemail\model\email\EmailService;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  Email
  |*****************************************************************************
  |
  | Controller Email
  | @author qDevTools,
  | @date 2020-02-16 16:32:43,
  |*****************************************************************************
 */

class EmailController extends \Qerana\core\QeranaC
{

    protected
            $_EmailService;

    public function __construct()
    {
        parent::__construct();
        $this->_EmailService = new EmailService;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all in json
     * -------------------------------------------------------------------------
     */
    public function getAllInJson()
    {
        return $this->_EmailService->getAll(true);
    }

    /**
     * -------------------------------------------------------------------------
     * Get one in json
     * -------------------------------------------------------------------------
     */
    public function getOneInJson(int $id)
    {
        return $this->_EmailService->getById($id, true);
    }

    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index(): void
    {


        $vars = [
            'Plugins' => [
                'data_json.js',
                'app/qemail/email.js'
            ]
        ];
        \Qerana\core\View::showView('email/index_email', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Add new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function add(): void
    {

        $vars = [
            'Accounts' => $this->_EmailService->getAccounts()
        ];

        \Qerana\core\View::showForm('email/add_email', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Save new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function save(): void
    {

        $this->_EmailService->save();
        \helpers\Redirect::toAction('detail/'.$this->_EmailService->Email->id_email);
    }

    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function detail(int $id): void
    {
        $vars = [
            'id_email' => $id,
            'Email' => $this->_EmailService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/qemail/email.js'
            ]
        ];

        \Qerana\core\View::showView('email/detail_email', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Edit 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function edit(int $id): void
    {

        $vars = [
            'Email' => $this->_EmailService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/qemail/email.js'
            ]
        ];
        \Qerana\core\View::showForm('email/edit_email', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * modify
     * -------------------------------------------------------------------------
     * @return void
     */
    public function modify(): void
    {
        $this->_EmailService->save();
    }

    /**
     * -------------------------------------------------------------------------
     * Delete
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        $this->_EmailService->delete($id);
        \helpers\Redirect::to('/qemail/email/index');
    }

    /**
     * Send email
     * @param int $id
     */
    public function send(int $id): void{
        // get Email
        $Email = $this->_EmailService->getById($id);
        $Email->send();
    }
}
