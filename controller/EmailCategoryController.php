<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\controller;

use Qerapp\qemail\model\email\EmailcategoryService;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  EmailCategory
  |*****************************************************************************
  |
  | Controller EmailCategory
  | @author qDevTools,
  | @date 2020-10-10 16:56:02,
  |*****************************************************************************
 */

class EmailCategoryController extends \Qerana\core\QeranaC {

    protected
            $_EmailcategoryService;

    public function __construct() {
        parent::__construct();
        $this->_EmailcategoryService = new EmailcategoryService;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all in json
     * -------------------------------------------------------------------------
     */
    public function getAllInJson() {
        return $this->_EmailcategoryService->getAll(true);
    }

    /**
     * -------------------------------------------------------------------------
     * Get one in json
     * -------------------------------------------------------------------------
     */
    public function getOneInJson(int $id) {
        return $this->_EmailcategoryService->getById($id, true);
    }

    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index(): void {


        $vars = [
            'Plugins' => [
                'data_json.js',
                'app/qemail/emailcategory.js'
            ]
        ];
        \Qerana\core\View::showView('emailcategory/index_emailcategory', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Add new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function add(): void {

        $vars = [];

        \Qerana\core\View::showForm('emailcategory/add_emailcategory', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Save new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function save(): void {

        $this->_EmailcategoryService->save();
    }

    
    
    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function detail(int $id): void {


        $vars = [
            'id_category' => $id,
            'EmailCategory' => $this->_EmailcategoryService->getById($id),
        ];

        \Qerana\core\View::showView('emailcategory/detail_emailcategory', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Edit 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function edit(int $id): void {

        $vars = [
            'EmailCategory' => $this->_EmailcategoryService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/qemail/emailcategory.js'
            ]
        ];
        \Qerana\core\View::showForm('emailcategory/edit_emailcategory', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * modify
     * -------------------------------------------------------------------------
     * @return void
     */
    public function modify(): void {
        $this->_EmailcategoryService->save();
    }

    /**
     * -------------------------------------------------------------------------
     * Delete
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function delete(int $id): void {
        $this->_EmailcategoryService->delete($id);
    }

}
