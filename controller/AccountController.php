<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\controller;

use Qerapp\qemail\model\account\AccountService,
    Qerapp\qemail\model\email\EmailcategoryService;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  Account
  |*****************************************************************************
  |
  | Controller Account
  | @author qDevTools,
  | @date 2020-02-14 06:06:28,
  |*****************************************************************************
 */

class AccountController extends \Qerana\core\QeranaC {

    protected
            $_AccountService;

    public function __construct() {
        parent::__construct();
        $this->_AccountService = new AccountService;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all in json
     * -------------------------------------------------------------------------
     */
    public function getAllInJson() {
        return $this->_AccountService->getAll(true);
    }

    /**
     * -------------------------------------------------------------------------
     * Get one in json
     * -------------------------------------------------------------------------
     */
    public function getOneInJson(int $id) {
        return $this->_AccountService->getById($id, true);
    }

    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index(): void {


        $vars = [
            'Plugins' => [
                'data_json.js',
                'app/qemail/account.js'
            ]
        ];
        \Qerana\core\View::showView('account/index_account', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Add new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function add(): void {

        $vars = [];

        \Qerana\core\View::showForm('account/add_account', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Save new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function save(): void {

        $this->_AccountService->save();
        \helpers\Redirect::to('/qemail/account/detail/' . $this->_AccountService->id_account);
    }

    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function detail(int $id): void {

        $vars = [
            'id_account' => $id,
            'Categories' => $this->_AccountService->getCategories(),
            'Account' => $this->_AccountService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/qemail/account.js'
            ]
        ];

        \Qerana\core\View::showView('account/detail_account', $vars);
    }

    /**
     * Activate task to a account
     * @param int $id_category
     * @param int $id_account
     */
    public function activateTaskAccount(int $account_id,int $id_category, int $id_account) {

        
        $EmailCategoryService = new EmailcategoryService;
        $EmailCategoryService->activateTask($id_category, $id_account);
        \helpers\Redirect::toAction('detail/' . $account_id);
    }

    /**
     * -------------------------------------------------------------------------
     * Edit 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function edit(int $id): void {
        $vars = [
            'Account' => $this->_AccountService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/qemail/account.js'
            ]
        ];
        \Qerana\core\View::showForm('account/edit_account', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * modify
     * -------------------------------------------------------------------------
     * @return void
     */
    public function modify(): void {
        $this->_AccountService->save();
        \helpers\Redirect::to('/qemail/account/detail/' . $this->_AccountService->id_account);
    }

    /**
     * -------------------------------------------------------------------------
     * test
     * -------------------------------------------------------------------------
     * @return void
     */
    public function test(int $id_account) {
        $result = $this->_AccountService->testAccount($id_account);
        if ($result == '1') {
            \QException\Exceptions::ShowSuccessful('TestEmail', 'TestMail passed!!');
        } else {
            \QException\Exceptions::ShowError('TestEmail', 'TestMail FAIL!!');
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Delete
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function delete(int $id): void {
        $this->_AccountService->delete($id);
        \helpers\Redirect::toAction('index');
    }

}
