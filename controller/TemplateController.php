<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\controller;

use Qerapp\qemail\model\email\TemplateService;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  Template
  |*****************************************************************************
  |
  | Controller Template
  | @author qDevTools,
  | @date 2020-02-27 06:40:17,
  |*****************************************************************************
 */

class TemplateController extends \Qerana\core\QeranaC
{

    protected
            $_TemplateService;

    public function __construct()
    {
        parent::__construct();
        $this->_TemplateService = new TemplateService;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all in json
     * -------------------------------------------------------------------------
     */
    public function getAllInJson()
    {
        return $this->_TemplateService->getAll(true);
    }

    /**
     * -------------------------------------------------------------------------
     * Get one in json
     * -------------------------------------------------------------------------
     */
    public function getOneInJson(int $id)
    {
        return $this->_TemplateService->getById($id, true);
    }

    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index(): void
    {


        $vars = [
            'Plugins' => [
                'data_json.js',
                'app/qemail/template.js'
            ]
        ];
        \Qerana\core\View::showView('template/index_template', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Add new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function add(): void
    {

        $vars = [];

        \Qerana\core\View::showForm('template/add_template', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Save new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function save(): void
    {

        $this->_TemplateService->save();
    }

    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function detail(int $id): void
    {


        $vars = [
            'id_qemail_tpl' => $id,
            'Template' => $this->_TemplateService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/qemail/template.js'
            ]
        ];

        \Qerana\core\View::showView('template/detail_template', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Edit 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function edit(int $id): void
    {

        $vars = [
            'Template' => $this->_TemplateService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/qemail/template.js'
            ]
        ];
        \Qerana\core\View::showForm('template/edit_template', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * modify
     * -------------------------------------------------------------------------
     * @return void
     */
    public function modify(): void
    {
        $this->_TemplateService->save();
        \helpers\Redirect::to('/qemail/template/detail/'.$this->_TemplateService->id_template);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        $this->_TemplateService->delete($id);
    }

}
