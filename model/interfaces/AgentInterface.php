<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\interfaces;
/**
 * *****************************************************************************
 * Description of AgentInterface
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
interface AgentInterface
{
    
    public function init();
    
    public function set_destinations(string $destinations);
    
    public function set_subject(string $subject);
    
    public function set_body(string $body);
    
    public function set_debug_level(int $level);
    
    public function set_attachments(array $attachments = []);
    
    public function send();
    
    
}
