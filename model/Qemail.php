<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model;

use Qerapp\qemail\model\interfaces\AgentInterface,
    Qerapp\qemail\model\account\interfaces\AccountInterface,
    Qerapp\qemail\model\email\interfaces\EmailInterface,
    Qerapp\qbasic\model\log\LogService,
    Qerapp\qemail\model\agents\PhpMailerAgent,
    Qerapp\qemail\model\email\repository\EmailRepository,
    Qerapp\qemail\model\email\mapper\EmailMapper;

/**
 * *****************************************************************************
 * Description of Qemail
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class Qemail
{

    public

    // para que guarde en la tabal de logs los emails
            $log = true;
    protected
            $_Agent,
            $_Account,
            $_Email;

    public function __construct()
    {
        
    }

    /**
     * Set account 
     * @param AccountInterface $Account
     */
    public function setAccount(AccountInterface $Account)
    {
        $this->_Account = $Account;
    }

    /**
     * Set agent
     * @param AgentInterface $Agent
     */
    public function setAgent(AgentInterface $Agent = null)
    {
        $this->_Agent = (is_null($Agent)) ? new PhpMailerAgent($this->_Account) : $Agent;
    }

    /**
     *  1= sended, 2= error, 0 = 
     * @param int $status
     */
    public function updateEmailStatus(int $status)
    {

        if ($this->log) {
            $this->_Email->sw_status = $status;
            $EmailRepository = new EmailRepository(new EmailMapper());
            $EmailRepository->store($this->_Email);
        }
    }

    /**
     * Send test email
     */
    public function sendTestEmail()
    {

        try {
            $this->_Agent->set_debug_level(5);
            $this->_Agent->set_subject('Qemail-test');
            $this->_Agent->set_body('Just testing!!');
            $this->_Agent->set_destinations($this->_Agent->Account->address);
            return $this->_Agent->send();
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Qemail.SendTestMail', $ex);
        }
    }

    /**
     * Send a Email
     * @param EmailInterface $Email
     */
    public function sendEmail(EmailInterface $Email)
    {

        try {

            $this->_Email = $Email;
            // set the account
            $this->_Account = $this->_Email->Account;

            // set agent
            //$this->setAgent();
            // set subject body and destinations

            $this->_Agent->set_destinations($this->_Email->destination);
            $this->_Agent->set_subject($this->_Email->subject);
            $this->_Agent->set_body($this->_Email->body);
            $this->_Agent->set_attachments($this->_Email->attachments);

            return $this->_send();
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Qemail.SendEmail', $ex);
        }
    }

    /**
     * Send Email and register log
     */
    private function _send()
    {

        // create logger
        $Logger = new LogService;
        $Logger->log_type = 'qemail_' . $this->_Email->id_email;

        $message = $this->_Agent->send();
        if ($message == 'Sended') {
            $Logger->sw_successfull = '1';
            $Logger->message_log = 'Sended!';
            $this->updateEmailStatus('1');
        } else {
            $Logger->sw_successfull = '0';
            $Logger->message_log = $message;
            $this->updateEmailStatus('2');
        }

        $Logger->registerLog();
        if ($message == 'Sended') {
            return true;
        } else {
            \QException\Exceptions::showError('Qemail.Error.Sending', $message);
        }
    }

}
