<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Qerapp\qemail\model\agents;

use Qerapp\qemail\model\interfaces\AgentInterface;

/**
 * Description of GmailAgent
 *
 * @author diemarc
 */
class GmailAgent implements AgentInterface
{

    private
            // unique id para enviar en los headers del email,
            //@TODO, no se para que es esto, es un unique_id en unique time, pero q
            // pasa si quitamos eso???
            $boundary,
            $boundary2,
            
            // @object conexion de gmail, se recibe por inyeccion
            $GmailService,
            
            // @object objeto que formatea los emails de gmail, el mensaje
            $GmailMessage,
            
            // @string , el mensaje del email, codificacion del email
            $raw_message = '';

    public function __construct($GmailService)
    {
        $this->GmailService = $GmailService;
        
        $this->GmailMessage = new \Google_Service_Gmail_Message();
        
        // setamos el boudnaries
        $this->boundary = uniqid("_Part_" . time(), true);
        $this->boundary2 = uniqid("_Part2_" . time(), true);
        
        
        $this->init();
    }

    /**
     * Initialize
     * @param string $sender
     */
    public function init()
    {

        $this->raw_message .= "From: Pruebas de oauth2\r\n";
    }

    /**
     * Envia un correo
     */
    public function send()
    {
        try {

            $rawMessage = strtr(base64_encode($this->raw_message), array('+' => '-', '/' => '_'));
            $this->GmailMessage->setRaw($rawMessage);

            $Response = $this->GmailService->users_messages->send('me', $this->GmailMessage);
            
            if($Response->labelIds[0] === 'SENT'){
                return 'Sended';
            }
            else{
                return false;
            }
            

            return $Response;
            
        } catch (\Exception $ex) {
            
        }
    }

    /**
     * Setea los adjuntos
     * @param array $attachments
     */
    public function set_attachments(array $attachments = [])
    {

        
        foreach ($attachments AS $attach):
            $nombre = $attach->name;
            // Parte del adjunto
            $this->raw_message .= "\n";
            $this->raw_message .= "--$this->boundary\n";
            $this->raw_message .= "Content-Transfer-Encoding: base64\n";
            $this->raw_message .= "Content-Type: {$attach->mime_type}; name=" . $nombre . ";\n";
            $this->raw_message .= "Content-Disposition: attachment; filename=" . $nombre . ";\n";
            $this->raw_message .= "\n";
            $this->raw_message .= $attach->archivo;
        endforeach;
    }

    /**
     * Setea el body
     * @param string $body
     */
    public function set_body(string $body)
    {
        $this->raw_message .= $body; //remove any HTML tags
        $this->raw_message .= "\n";
    }

    public function set_debug_level(int $level)
    {
        
    }

    /**
     * Set destination
     * @param string $destinations
     */
    public function set_destinations(string $destinations)
    {

        $this->raw_message .= "To: <{$destinations}>\r\n";
    }

    /**
     * Set subject
     * @param string $subject
     */
    public function set_subject(string $subject)
    {
        $this->raw_message .= 'Subject: =?utf-8?B?' . base64_encode($subject) . "?=\r\n";
        $this->raw_message .= "MIME-Version: 1.0\r\n";
        $this->raw_message .= "Content-Type: multipart/mixed;\n";
        $this->raw_message .= " boundary=\"$this->boundary\"\n";
        $this->raw_message .= "\n";
        $this->raw_message .= "--$this->boundary\n";
        $this->raw_message .= "Content-Type: multipart/alternative;\n";
        $this->raw_message .= " boundary=\"$this->boundary2\"\n";
        $this->raw_message .= "\n";
        $this->raw_message .= "--$this->boundary2\n";

        // Parte de texto plano
        $this->raw_message .= "Content-Type: text/html; charset=utf-8\n";
        $this->raw_message .= "Content-Transfer-Encoding: 7bit\n";
        $this->raw_message .= "\n";
    }

}
