<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\agents;

use \PHPMailer\PHPMailer\PHPMailer,
    \PHPMailer\PHPMailer\Exception AS PhpMailerException,
    Qerapp\qemail\model\interfaces\AgentInterface,
    Qerapp\qemail\model\account\interfaces\AccountInterface;

/**
 * *****************************************************************************
 * Description of PhpMailerAgent
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class PhpMailerAgent implements AgentInterface {

    public
            $add_ssl_exception = true,    
            $debug_level,
            $Account;
    protected
            $_PhpMailer,
            $_attachments = [];

    public function __construct(AccountInterface $Account, $debug_level = 0) {
        $this->Account = $Account;
        $this->_PhpMailer = new PHPMailer(true);
        $this->set_debug_level($debug_level);
        $this->init();
    }

    /**
     * Initialize phpmailer settings configuration
     */
    public function init() {
        try {

            $this->_PhpMailer->Timeout = 15;
            $this->_PhpMailer->SMTPDebug = $this->debug_level;
            $this->_PhpMailer->isSMTP();
            // agregamos una configuracion adicional , aplicar esto si falla la conexion al host de smtp
            if($this->add_ssl_exception){
                  $this->_PhpMailer->SMTPOptions =[
                    'ssl' => [
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    ]
                ]; 
            }


            $this->_PhpMailer->Host = $this->Account->smtp_server;
            $this->_PhpMailer->SMTPAuth = $this->Account->smtp_auth;
            $this->_PhpMailer->Username = $this->Account->username;
            $this->_PhpMailer->Password = $this->Account->get_decrypt_pass();

            // ssl or startls , etc are required
            if ($this->Account->smtp_secure != '') {
                $this->_PhpMailer->SMTPSecure = $this->Account->smtp_secure;
            }

            $this->_PhpMailer->Port = $this->Account->smtp_port;
            $this->_PhpMailer->From = $this->Account->address;
            $this->_PhpMailer->isHTML(true);
            $this->_PhpMailer->FromName = $this->Account->from_name;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowError('PhpMailer.Agent.Init',  $ex->getMessage());
        }
    }

    /**
     * Send the email
     */
    public function send() {
        
        if(!$this->_PhpMailer->send()){
            return $this->_PhpMailer->ErrorInfo;
        }else{
            return true;
        }
        
    }

    /**
     * Set attachments
     * @param array $attachments
     */
    public function set_attachments(array $attachments = []) {

        foreach ($attachments as $Attachment):
            $this->_PhpMailer->addAttachment(__DATA__ . '/files/' . $Attachment->path_attachment);
        endforeach;
    }

    /**
     * set destinations
     * @param string $destinations
     * @throws \RuntimeException
     */
    public function set_destinations(string $destinations) {

        // convert to array 
        $addresses = explode(';', $destinations);

        foreach ($addresses AS $address):

            // check destination email
            if (!filter_var($address, FILTER_VALIDATE_EMAIL)) {
                throw new \RuntimeException($address . ' is not valid email!!');
            } else {
                $this->_PhpMailer->addAddress($address);
            }

        endforeach;
    }

    /**
     * Set the subject
     * @param string $subject
     */
    public function set_subject(string $subject) {
        $this->_PhpMailer->Subject = filter_var($subject, FILTER_SANITIZE_STRING);
    }

    /**
     * Set the body 
     * @param string $body
     */
    public function set_body(string $body) {
        $this->_PhpMailer->Body = $body;
    }

    /**
     * Set debug level
     * @param int $level
     */
    public function set_debug_level(int $level) {
        $this->debug_level = $level;
    }

}
