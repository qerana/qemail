<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\account\interfaces;

/*
  |*****************************************************************************
  | INTERFACE  for [{folder_name}]
  |*****************************************************************************
  |
  | Entity [{folder_name}]
  | @author qDevTools,
  | @date 2020-02-14 06:04:59,
  |*****************************************************************************
 */

interface AccountInterface
{

//SETTERS

    public function set_id_account(int $id_mail_account): void;

    public function set_account(string $account): void;

    public function set_address(string $address): void;

    public function set_username(string $username): void;

    public function set_encrypt_pass();

    public function set_password(string $password): void;

    public function set_smtp_server(string $smtp_server): void;

    public function set_smtp_auth(string $smtp_auth): void;

    public function set_smtp_port(int $mail_smtp_port): void;

    public function set_from_name(string $from_name): void;

    public function set_smtp_secure(string $smtp_secure): void;

//GETTERS

    public function get_id_account();

    public function get_account();

    public function get_address();

    public function get_username();

    public function get_password();

    public function get_decrypt_pass();

    public function get_smtp_server();

    public function get_smtp_auth();

    public function get_smtp_port();

    public function get_from_name();

    public function get_smtp_secure();
}
