<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\account\interfaces;

/*
  |*****************************************************************************
  | INTERFACE  for [{folder_name}]
  |*****************************************************************************
  |
  | Entity [{folder_name}]
  | @author qDevTools,
  | @date 2020-02-14 06:04:59,
  |*****************************************************************************
 */

interface GoogleClientInterface
{

//SETTERS

    public function set_application_name(string $application_name): void;

    public function set_scope(string $scope): void;

    public function set_credential(string $credential): void;

    public function set_access_type(string $access_type): void;

    public function set_prompt(string $prompt);

    public function set_token(string $token);

//GETTERS

    public function get_application_name();

    public function get_scope();

    public function get_credential();

    public function get_access_type();

    public function get_prompt();

    public function get_token();
}
