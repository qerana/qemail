<?php

namespace Qerapp\qemail\model\account\google;

use Qerapp\qemail\model\account\interfaces\GoogleClientInterface;
use Google_Client,
    Google_Service_Gmail;

/**
 * Description of GoogleClient
 *
 * @author diemarc
 */
class GoogleClient
{

    public
            $Google,
            $GoogleClientConfig;

    public function __construct(GoogleClientInterface $GoogleClient)
    {
        $this->GoogleClientConfig = $GoogleClient;
        $this->setupClient();
        $this->checkGmailToken();
    }

    /**
     *  Inicializa el cliente de google
     */
    public function setupClient()
    {

        try {
            $this->Google = new Google_Client();
            $this->Google->setApplicationName($this->GoogleClientConfig->application_name);
            $this->Google->setScopes($this->GoogleClientConfig->scope);
            $this->Google->setAuthConfig($this->GoogleClientConfig->credential);
            $this->Google->setAccessType($this->GoogleClientConfig->access_type);
            $this->Google->setPrompt($this->GoogleClientConfig->prompt);
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Error al configurar los servicios de google', $ex);
        }
    }

    private function checkGmailToken()
    {
        try {

            // Load previously authorized token from a file, if it exists.
            // The file token.json stores the user's access and refresh tokens


            if (file_exists($this->GoogleClientConfig->token)) {
                $AccessToken = json_decode(file_get_contents($this->GoogleClientConfig->token), true);

                $this->Google->setAccessToken($AccessToken);
            }

            $this->refreshGmailToken();
        } catch (\Exception $e) {
            \QException\Exceptions::ShowException('Error al checkear el token de google', $e);
        }
    }

//    /**
//     *  Crea el token .js
//     */
//    private function createFirstToken()
//    {
//
//        // obtenemos una url
////        $url = $this->Google->createAuthUrl();
////
////        echo $url;
////        die();
//        $code = '4/0AY0e-g6WPjG8xj18ZLucEgo2VBFVZWP59DwTwcNLXWLrXaW01F0wQmcQrS3auHOUgLV5BA';
//        $AccessToken = $this->Google->fetchAccessTokenWithAuthCode($code);
//
//        $this->Google->setAccessToken($AccessToken);
//
//        // Check to see if there was an error.
//        if (array_key_exists('error', $AccessToken)) {
//            throw new \Exception(join(', ', $AccessToken));
//        }
//    }

    /**
     * Obtiene un nuevo token
     */
    private function refreshGmailToken()
    {

        try {

            if ($this->Google->isAccessTokenExpired()) {
                // Refresh the token if possible.
                if ($this->Google->getRefreshToken()) {
                    $this->Google->fetchAccessTokenWithRefreshToken($this->Google->getRefreshToken());
                } else {
                   // $url = $this->Google->createAuthUrl();
                    //echo 'visita esto:' . $url;
                    $code = '4/0AY0e-g5ueKoVjKmU1Om_WrgatYNY0C5zGpYIYclLr5zAu_5QYC6AdLazvjDQERNuHjtxQw';
                    $AccessToken = $this->Google->fetchAccessTokenWithAuthCode($code);
                    
                    $this->Google->setAccessToken($AccessToken);

                }
            }

            // Save the token to a file.
            if (!file_exists(dirname($this->GoogleClientConfig->token))) {
                mkdir(dirname($this->GoogleClientConfig->token), 0700, true);
            }
            file_put_contents($this->GoogleClientConfig->token, json_encode($this->Google->getAccessToken()));
        } catch (Exception $e) {
            \QException\Exceptions::ShowException('Error al refresh el token de google', $e);
        }
    }

}
