<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Qerapp\qemail\model\account\entity;

use \Ada\EntityManager;
use Qerapp\qemail\model\account\interfaces\GoogleClientInterface;

/**
 * Description of GoogleClientEntity
 *
 * @author diemarc
 */
class GoogleClientEntity extends EntityManager implements GoogleClientInterface, \JsonSerializable
{

    protected
            $_token = 'token.json',
            $_access_type = 'offline',
            $_application_name = 'gesfor1',
            $_credential = 'credentials.json',
            $_prompt = 'select_account consent',
            
            // create messages, send emails, 
            $_scope = 'https://mail.google.com/',
            // @param string, carpeta donde se guardaran las credenciales
            // @path _data_/keys/gapi/{account}
            $_account;

    public function __construct(string $account)
    {
        $this->set_account($account);
        $this->set_credential('credentials.json');
        $this->set_token('token.json');
        
    }

    public function set_access_type(string $access_type): void
    {
        $this->_access_type = $access_type;
    }

    public function set_application_name(string $application_name): void
    {
        $this->_application_name = $application_name;
    }

    public function set_credential(string $credential): void
    {
        $this->_credential = $this->_account.'/'.$credential;
    }

    public function set_prompt(string $prompt)
    {
        $this->_prompt = $prompt;
    }

    public function set_scope(string $scope): void
    {
        $this->_scope = $scope;
    }

    public function set_token(string $token)
    {
        $this->_token = $this->_account.'/'.$token;
    }

    public function get_access_type()
    {
        return $this->_access_type;
    }

    public function get_application_name()
    {
        return $this->_application_name;
    }

    public function get_credential()
    {
        return $this->_credential;
    }

    public function get_prompt()
    {
        return $this->_prompt;
    }

    public function get_scope()
    {
        return $this->_scope;
    }

    public function get_token()
    {
        return $this->_token;
    }

    /**
     * Setea la carpeta donde se guardaran las configuraciones de la cienta
     * @param string $account
     */
    public function set_account(string $account)
    {

        $path_account = __DATA__ . 'keys/gapi/' . $account;

        if (!is_dir($path_account)) {
            mkdir($path_account, 0700, true);
        }
        
        $this->_account = $path_account;
    }

}
