<?php

/*
 * Copyright (C) 2019/20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\account\entity;

use \Ada\EntityManager,
    Qerapp\qemail\model\account\interfaces\AccountInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for Account
  |*****************************************************************************
  |
  | Entity Account
  | @author qDevTools,
  | @date 2020-02-14 06:04:59,
  |*****************************************************************************
 */

class AccountEntity extends EntityManager implements AccountInterface, \JsonSerializable {

    protected

    //ATRIBUTES        

    /** @var int(11), $id_mail_account  */
            $_id_account,
            /** @var varchar(45), $account  */
            $_account,
            /** @var varchar(150), $mail_address  */
            $_address,
            /** @var varchar(150), $mail_username  */
            $_username,
            /** @var text(), $mail_password  */
            $_password,
            /** @var varchar(150), $mail_smtp_server  */
            $_smtp_server,
            /** @var tinyint(1), $mail_smtp_auth  */
            $_smtp_auth,
            /** @var int(2), $mail_smtp_port  */
            $_smtp_port,
            /** @var varchar(45), $mail_from_name  */
            $_from_name,
            /** @var varchar(20), $smtp_secure  */
            $_smtp_secure,
            $_id_user,
            $_id_task;
    //RELATED

    public
    /** numf of email sended  */
            $num_emails,
            /** Categories associated */
            $Categories;

    public function __construct(array $data = []) {

        $this->populate($data);
        //RELATED-LOAD
    }

    public function setNumberEmails() {

        $sended = \Ada\EntityRelation::hasMany($this, 'email', 'id_account');
        $this->num_emails = count($sended);
    }

    /**
     *  Set categories
     */
    public function setCategories() {
        $this->Categories = \Ada\EntityRelation::hasMany($this, 'emailcategory', 'id_account');
    }

//SETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_mail_account
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_account($id_account): void {

        if (!empty($id_account)) {
            try {
                $this->_id_account = filter_var($id_account, FILTER_SANITIZE_STRING);
            } catch (\Exception $ex) {
                \QException\Exceptions::ShowException("Account.Error.id_account", $ex);
            }
        }
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_user
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_user($id_user): void {

        if (!empty($id_user)) {
            try {
                $this->_id_user = filter_var($id_user, FILTER_SANITIZE_STRING);
            } catch (\Exception $ex) {
                \QException\Exceptions::ShowException("Account.Error.id_user", $ex);
            }
        }
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for account
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_account($account = ""): void {
        $this->_account = filter_var($account, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for mail_address
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_address($mail_address = ""): void {
        if (!empty($mail_address)) {
            try {
                $this->_address = \helpers\Utils::checkEmail($mail_address);
            } catch (\Exception $ex) {
                \QException\Exceptions::ShowException("Account.Error.mail_address", $ex);
            }
        }
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for mail_username
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_username($mail_username = ""): void {
        if (!empty($mail_username)) {
            try {
                $this->_username = filter_var($mail_username, FILTER_SANITIZE_STRING);
            } catch (\Exception $ex) {
                \QException\Exceptions::ShowException("Account.Error.mail_username", $ex);
            }
        }
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for mail_password
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_password($mail_password = ""): void {
        // $this->_mail_password = \helpers\Security::encrypt(filter_var($mail_password, FILTER_SANITIZE_STRING));
        $this->_password = filter_var($mail_password, FILTER_SANITIZE_STRING);
    }

    public function set_encrypt_pass() {
        $this->_password = \helpers\Security::encrypt($this->_password);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for mail_smtp_server
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_smtp_server($mail_smtp_server = ""): void {
        if (!empty($mail_smtp_server)) {
            try {
                $this->_smtp_server = filter_var($mail_smtp_server, FILTER_SANITIZE_STRING);
            } catch (\Exception $ex) {
                \QException\Exceptions::ShowException("Account.Error.mail_smtp_server", $ex);
            }
        }
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for mail_smtp_auth
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_smtp_auth($mail_smtp_auth = ""): void {
        if (!empty($mail_smtp_auth)) {
            try {
                $this->_smtp_auth = filter_var($mail_smtp_auth, FILTER_SANITIZE_STRING);
            } catch (\Exception $ex) {
                \QException\Exceptions::ShowException("Account.Error.smtp_auth", $ex);
            }
        }
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for mail_smtp_port
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_smtp_port($mail_smtp_port = 0): void {
        if (!empty($mail_smtp_port)) {
            try {
                $this->_smtp_port = filter_var($mail_smtp_port, FILTER_SANITIZE_NUMBER_INT);
            } catch (\Exception $ex) {
                \QException\Exceptions::ShowException("Account.Error.mail_smtp_port", $ex);
            }
        }
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for mail_from_name
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_from_name($mail_from_name = ""): void {
        if (!empty($mail_from_name)) {
            try {
                $this->_from_name = filter_var($mail_from_name, FILTER_SANITIZE_STRING);
            } catch (\Exception $ex) {
                \QException\Exceptions::ShowException("Account.Error.mail_form_name", $ex);
            }
        }
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for smtp_secure
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_smtp_secure($smtp_secure = ""): void {
        if (!empty($smtp_secure)) {
            try {
                $this->_smtp_secure = filter_var($smtp_secure, FILTER_SANITIZE_STRING);
            } catch (\Exception $ex) {
                \QException\Exceptions::ShowException("Account.Error.smtp_secure", $ex);
            }
        }
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for mail_smtp_server
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_id_task($id_task = ""): void {
        if (!empty($id_task)) {
            try {
                $this->_id_task = filter_var($id_task, FILTER_SANITIZE_STRING);
            } catch (\Exception $ex) {
                \QException\Exceptions::ShowException("Account.Error.id_task", $ex);
            }
        }
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_mail_account
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_account() {
        return $this->_id_account;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_mail_user
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_user() {
        return $this->_id_user;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for account
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_account() {
        return filter_var($this->_account, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for mail_address
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_address() {
        return filter_var($this->_address, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for mail_username
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_username() {
        return filter_var($this->_username, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for mail_password
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_password() {
        return filter_var($this->_password, FILTER_SANITIZE_STRING);
    }

    /**
     * Get the decrypt password
     * @return type
     */
    public function get_decrypt_pass() {
        return \helpers\Security::decrypt($this->_password);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for mail_smtp_server
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_smtp_server() {
        return filter_var($this->_smtp_server, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for mail_smtp_auth
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_id_task() {
        return $this->_id_task;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for mail_smtp_auth
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_smtp_auth() {
        return $this->_smtp_auth;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for mail_smtp_port
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_smtp_port() {
        return $this->_smtp_port;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for mail_from_name
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_from_name() {
        return filter_var($this->_from_name, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for smtp_secure
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_smtp_secure() {
        return filter_var($this->_smtp_secure, FILTER_SANITIZE_STRING);
    }

}
