<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\account\repository;

use Qerapp\qemail\model\account\interfaces\AccountMapperInterface,
    Qerapp\qemail\model\account\interfaces\AccountRepositoryInterface,
    Qerapp\qemail\model\account\interfaces\AccountInterface;

/*
  |*****************************************************************************
  | AccountRepositoryRepository
  |*****************************************************************************
  |
  | Repository AccountRepository
  | @author qDevTools,
  | @date 2020-02-14 06:04:59,
  |*****************************************************************************
 */

class AccountRepository implements AccountRepositoryInterface
{

    private
            $_AccountMapper;

    public function __construct(AccountMapperInterface $Mapper)
    {

        $this->_AccountMapper = $Mapper;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all AccountRepository
     * -------------------------------------------------------------------------
     * @return AccountRepositoryEntity collection
     */
    public function findById(int $id)
    {
        return $this->_AccountMapper->findOne(['id_account' => $id]);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all AccountRepository
     * -------------------------------------------------------------------------
     * @return AccountRepositoryEntity collection
     */
    public function findAll(array $conditions = [], array $options = [])
    {
        return $this->_AccountMapper->findAll($conditions, $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_account
     * ------------------------------------------------------------------------- 
     * @param id_account 
     */
    public function findById_account(int $id_account, array $options = [])
    {
        return $this->_AccountMapper->findAll(['id_account' => $id_account], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  account
     * ------------------------------------------------------------------------- 
     * @param account 
     */

    public function findByAccount(string $account, array $options = [])
    {
        return $this->_AccountMapper->findAll(['account' => $account], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  mail_address
     * ------------------------------------------------------------------------- 
     * @param mail_address 
     */

    public function findByAddress(string $mail_address, array $options = [])
    {
        return $this->_AccountMapper->findAll(['mail_address' => $mail_address], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  mail_username
     * ------------------------------------------------------------------------- 
     * @param mail_username 
     */

    public function findByUsername(string $mail_username, array $options = [])
    {
        return $this->_AccountMapper->findAll(['mail_username' => $mail_username], $options);
    }


/**
     * ------------------------------------------------------------------------- 
     * Fin by  mail_smtp_server
     * ------------------------------------------------------------------------- 
     * @param mail_smtp_server 
     */

    public function findBySmtp_server(string $mail_smtp_server, array $options = [])
    {
        return $this->_AccountMapper->findAll(['mail_smtp_server' => $mail_smtp_server], $options);
    }


    /**
     * -------------------------------------------------------------------------
     * Save AccountRepository
     * -------------------------------------------------------------------------
     * @object $AccountRepositoryEntity
     * @return type
     */
    public function store(AccountInterface $AccountRepositoryEntity)
    {
        return $this->_AccountMapper->save($AccountRepositoryEntity);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete AccountRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_AccountMapper->delete($id);
    }

}
