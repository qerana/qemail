<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\account\mapper;

use Ada\adapters\AdapterInterface,
    Ada\adapters\PDOAdapter,
    Ada\adapters\XmlAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qemail\model\account\entity\AccountEntity,
    Qerapp\qemail\model\account\interfaces\AccountMapperInterface,
    Qerapp\qemail\model\account\interfaces\AccountInterface;

//RELATED-NAMESPACES    

/*
  |*****************************************************************************
  | MAPPER CLASS for AccountMapperEntity
  |*****************************************************************************
  |
  | MAPPER AccountMapper
  | @author qDevTools,
  | @date 2020-02-14 06:04:59,
  |*****************************************************************************
 */

class AccountMapper extends AdaDataMapper implements AccountMapperInterface
{

    //RELATED-MAPPERS

    public function __construct(AdapterInterface $Adapter = null)
    {
        
        $this->_Adapter = (is_null($Adapter) ) ? new PDOAdapter(\Ada\SqlPDO::singleton(), 'qemail_accounts') : $Adapter;
        parent::__construct($this->_Adapter);

        //RELATED-MAPPER-OBJECT
    }

    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */

    public function findById(int $id)
    {
        $row = $this->_Adapter->find(['id_account' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(AccountInterface $Account)
    {

        $data = parent::getDataObject($Account);

        
        if (is_null($Account->id_account)) {
            $Account->id_account = $this->_Adapter->insert($data);
        } else {
            
            $this->_Adapter->update($data, ['id_account' => $Account->id_account]);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($id)
    {

        // if $id is a object and a UserEntity  
        if ($id instanceof AccountInterface) {
            $id = $id->id_account;
        }

        return $this->_Adapter->delete(['id_account' => $id]);
    }

    /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): AccountEntity
    {
   
        $AccountEntity = new AccountEntity($row);
        return $AccountEntity;
    }

}
