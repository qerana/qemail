<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\account;

use Qerapp\qemail\model\account\entity\AccountEntity,
    Qerapp\qemail\model\account\mapper\AccountMapper,
    Qerapp\qemail\model\account\interfaces\AccountMapperInterface,
    Qerapp\qemail\model\account\repository\AccountRepository,
    Qerapp\qemail\model\Qemail,
    Qerapp\qemail\model\email\EmailcategoryService;

//RELATED-MAPPERS

/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity AccountService
  | @author qDevTools,
  | @date 2020-02-14 06:04:59,
  |*****************************************************************************
 */

class AccountService
{

    public
    //RELATED-MAPPER-OBJECT
            $AccountRepository,
            $id_account;

    public function __construct(AccountMapperInterface $Mapper = null)
    {

        $MapperRepository = (is_null($Mapper)) ? new AccountMapper() : $Mapper;
        $this->AccountRepository = new AccountRepository($MapperRepository);
    }

    
    public function getCategories(){
        
        $CategoryService = new EmailcategoryService;     
        return  $CategoryService->getAll();
        
    }
    
    
    
    
    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
     public function getAll($json = false)
    {

        $Accounts = $this->AccountRepository->findAll();

        foreach($Accounts AS $Account):
            $Account->setCategories();
        endforeach;
        
        if ($json) {
            
            echo json_encode($Accounts);
        } else {
            return $Accounts;
        }
    }

    /**
    * Obtiene una cuenta
    */
     public function getAccount(int $id){
         $Entity = $this->AccountRepository->findById($id);
         
         if($Entity instanceof interfaces\AccountInterface){
             return $Entity;
         }else{
                \QException\Exceptions::showHttpStatus(404, 'Mail not found!');
         }
    }

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false)
    {

        $Entity = $this->AccountRepository->findById($id);

        if ($Entity) {
            $Entity->setNumberEmails();
            $Entity->setCategories();
            if ($json) {
                echo json_encode($Entity);
            } else {
                return $Entity;
            }
        }
        else{
            \QException\Exceptions::showHttpStatus(404, 'Mail not found!');
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save 
     * -------------------------------------------------------------------------
     */
    public function save(array $data = [])
    {

        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;


        $Account = new AccountEntity($data_to_save);
        $Account->set_encrypt_pass();
        $this->AccountRepository->store($Account);
        $this->id_account = $Account->id_account;
    }

    /**
     * -------------------------------------------------------------------------
     * Delete 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function delete(int $id)
    {
        return $this->AccountRepository->remove($id);
    }

    
    /**
     * Perform a email test
     */
    public function testAccount(int $id_account){
        
        $Account = $this->getById($id_account);
        $Qemail = new Qemail();
        $Qemail->setAccount($Account);
        $Qemail->setAgent();
        return $Qemail->sendTestEmail();
        
    }
    
}
