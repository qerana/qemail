<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email;

use 
    Qerapp\qemail\model\email\entity\TemplateEntity,
    Qerapp\qemail\model\email\mapper\TemplateMapper,
    Qerapp\qemail\model\email\interfaces\TemplateMapperInterface,
    Qerapp\qemail\model\email\repository\TemplateRepository;

        //RELATED-MAPPERS
        
/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity TemplateService
  | @author qDevTools,
  | @date 2020-02-27 06:39:30,
  |*****************************************************************************
 */

class TemplateService
{
    
    public
            //RELATED-MAPPER-OBJECT
            $TemplateRepository,
            $id_template;
            
    public function __construct(TemplateMapperInterface $Mapper = null)
    {
        
         //RELATED-MAPPER-OBJECT-NEW
        
        try{
             $TemplateMapper = new TemplateMapper;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.TemplateService',$ex);
        }
        
         $MapperRepository = (is_null($Mapper)) ? $TemplateMapper : $Mapper;
         $this->TemplateRepository = new TemplateRepository($MapperRepository);
        
    }
    /**
     * -------------------------------------------------------------------------
     * simple test
     * -------------------------------------------------------------------------
     */
    public function runTest()
    {


        echo '<h1>Test</h1>';

       
    }
    
    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false){
        
        $Collection = $this->TemplateRepository->findAll(); 
        if($json){
            echo json_encode($Collection);
        }else{
            return $Collection;
        }
        
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id,$json = false){
        
        $Entity = $this->TemplateRepository->findById($id);
        if($Entity){
            if($json){
            echo json_encode($Entity);
        }else{
            return $Entity;
        }
        }else{
            \QException\Exceptions::showHttpStatus(404, 'Template '.$id.' NOT FOUND!!');
        }
        
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Save 
     * -------------------------------------------------------------------------
     */
   
    public function save(array $data = []){
        
        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;
        
        $Template = new TemplateEntity($data_to_save);
        $this->id_template = $Template->id_qemail_tpl;
        $this->TemplateRepository->store($Template);
        
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function delete(int $id){
        return $this->TemplateRepository->remove($id);
    }
    
 
}