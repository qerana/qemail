<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\mapper;

use Ada\adapters\AdapterInterface,
    Ada\adapters\PDOAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qemail\model\email\entity\MailattachmentEntity,
    Qerapp\qemail\model\email\interfaces\MailattachmentMapperInterface,
    Qerapp\qemail\model\email\interfaces\MailattachmentInterface;

//RELATED-NAMESPACES    

/*
  |*****************************************************************************
  | MAPPER CLASS for MailattachmentMapperEntity
  |*****************************************************************************
  |
  | MAPPER MailattachmentMapper
  | @author qDevTools,
  | @date 2020-02-18 06:31:59,
  |*****************************************************************************
 */

class MailattachmentMapper extends AdaDataMapper implements MailattachmentMapperInterface
{

    //RELATED-MAPPERS

    public function __construct(AdapterInterface $Adapter = null)
    {

        $this->_Adapter = (is_null($Adapter) ) ? new PDOAdapter(\Ada\SqlPDO::singleton(), 'qemail_attachments') : $Adapter;

        //RELATED-MAPPER-OBJECT


        parent::__construct($this->_Adapter);
    }

    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */

    public function findById(int $id)
    {
        $row = $this->_Adapter->find(['id_email_attachment' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(MailattachmentInterface $Mailattachment)
    {

        $data = parent::getDataObject($Mailattachment);

        if (is_null($Mailattachment->id_email_attachment)) {
            $Mailattachment->id_email_attachment = $this->_Adapter->insert($data);
        } else {
            $this->_Adapter->update($data, ['id_email_attachment' => $Mailattachment->id_email_attachment]);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($id)
    {

        // if $id is a object and a UserEntity  
        if ($id instanceof MailattachmentInterface) {
            $id = $id->id;
        }

        return $this->_Adapter->delete(['id_email_attachment' => $id]);
    }

    /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): MailattachmentEntity
    {
        $MailattachmentEntity = new MailattachmentEntity($row);
        return $MailattachmentEntity;
    }

}
