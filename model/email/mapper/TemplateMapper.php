<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\mapper;

use Ada\adapters\AdapterInterface,
    Ada\adapters\XmlAdapter,
    Ada\adapters\PDOAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qemail\model\email\entity\TemplateEntity,
    Qerapp\qemail\model\email\interfaces\TemplateMapperInterface,
    Qerapp\qemail\model\email\interfaces\TemplateInterface;

//RELATED-NAMESPACES    

/*
  |*****************************************************************************
  | MAPPER CLASS for TemplateMapperEntity
  |*****************************************************************************
  |
  | MAPPER TemplateMapper
  | @author qDevTools,
  | @date 2020-02-27 06:39:30,
  |*****************************************************************************
 */

class TemplateMapper extends AdaDataMapper implements TemplateMapperInterface
{

    //RELATED-MAPPERS

    public function __construct(AdapterInterface $Adapter = null)
    {

        $xml_template = __DATA__ . 'xml/qemail/templates';
        $this->_Adapter = (is_null($Adapter) ) ? new XmlAdapter($xml_template, 'template' ,false) : $Adapter;

        //RELATED-MAPPER-OBJECT


        parent::__construct($this->_Adapter);
    }

    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */

    public function findById(int $id)
    {
        $row = $this->_Adapter->find(['id_qemail_tpl' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(TemplateInterface $Template)
    {

        $data = parent::getDataObject($Template);

        if (is_null($Template->id_qemail_tpl)) {
            $Template->id_qemail_tpl = $this->_Adapter->insert($data);
        } else {
            $this->_Adapter->update($data, ['id_qemail_tpl' => $Template->id_qemail_tpl]);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($Template)
    {

        // if $id is a object and a UserEntity  
        if ($Template instanceof TemplateInterface) {
            $Template = $Template->id_qemail_tpl;
        }

        return $this->_Adapter->delete(['id_qemail_tpl' => $Template]);
    }

    /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): TemplateEntity
    {
        $TemplateEntity = new TemplateEntity($row);
        return $TemplateEntity;
    }

}
