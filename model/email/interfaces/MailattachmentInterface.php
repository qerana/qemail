<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\interfaces;
/*
  |*****************************************************************************
  | INTERFACE  for [{folder_name}]
  |*****************************************************************************
  |
  | Entity [{folder_name}]
  | @author qDevTools,
  | @date 2020-02-18 06:31:59,
  |*****************************************************************************
 */

interface MailattachmentInterface
{

    
//SETTERS

  public function set_id_email_attachment(int  $id_email_attachment):void; 
 public function set_id_email(int  $id_email):void; 
 public function set_path_attachment(string $path_attachment):void; 

    
//GETTERS

  public function get_id_email_attachment();
 public function get_id_email();
 public function get_path_attachment();

 
 
 
}