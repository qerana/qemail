<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\interfaces;
/*
  |*****************************************************************************
  | INTERFACE  for [{folder_name}]
  |*****************************************************************************
  |
  | Entity [{folder_name}]
  | @author qDevTools,
  | @date 2020-02-27 06:39:30,
  |*****************************************************************************
 */

interface TemplateInterface
{

    
//SETTERS

  public function set_id_qemail_tpl(int  $id_qemail_tpl):void; 
 public function set_tpl_name(string $tpl_name):void; 
 public function set_tpl_type(string $tpl_type):void; 
 public function set_tpl_content(string $tpl_content):void; 
 public function set_tpl_active(int  $tpl_active):void; 
 public function set_tpl_settings(string $tpl_settings):void; 

    
//GETTERS

  public function get_id_qemail_tpl();
 public function get_tpl_name();
 public function get_tpl_type();
 public function get_tpl_content();
 public function get_tpl_active();
 public function get_tpl_settings();

 
 
 
}