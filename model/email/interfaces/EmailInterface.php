<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\interfaces;
/*
  |*****************************************************************************
  | INTERFACE  for [{folder_name}]
  |*****************************************************************************
  |
  | Entity [{folder_name}]
  | @author qDevTools,
  | @date 2020-02-16 16:30:05,
  |*****************************************************************************
 */

interface EmailInterface
{

    
//SETTERS

  public function set_id_email(int  $id_email):void; 
 public function set_id_account(int  $id_mail_account):void; 
 public function set_destination(string $destination):void; 
 public function set_bcc(string $bcc):void; 
 public function set_subject(string $subject):void; 
 public function set_body(string $body):void; 
 public function set_created_at(string $created_at):void; 
 public function set_created_by(string $created_by):void; 
 public function set_sw_status(int  $sw_status):void; 

    
//GETTERS

  public function get_id_email();
 public function get_id_account();
 public function get_destination();
 public function get_bcc();
 public function get_subject();
 public function get_body();
 public function get_created_at();
 public function get_created_by();
 public function get_sw_status();

 
 
 
}