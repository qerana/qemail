<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\mapper;

use 
    Ada\adapters\AdapterInterface,
    Ada\adapters\PDOAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qemail\model\email\entity\EmailcategoryEntity,
    Qerapp\qemail\model\email\interfaces\EmailcategoryMapperInterface,
    Qerapp\qemail\model\email\interfaces\EmailcategoryInterface;
    
//RELATED-NAMESPACES    
    
/*
  |*****************************************************************************
  | MAPPER CLASS for EmailcategoryMapperEntity
  |*****************************************************************************
  |
  | MAPPER EmailcategoryMapper
  | @author qDevTools,
  | @date 2020-10-10 16:51:03,
  |*****************************************************************************
 */

class EmailcategoryMapper extends AdaDataMapper implements EmailcategoryMapperInterface  {

    //RELATED-MAPPERS
    
     public function __construct(AdapterInterface $Adapter = null)
    {

          $this->_Adapter = (is_null($Adapter) ) ? new PDOAdapter(\Ada\SqlPDO::singleton(),'qemail_categories') : $Adapter;
         
         //RELATED-MAPPER-OBJECT
         
         
        parent::__construct($this->_Adapter);
        
    }
    
    
    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */
    public function findById(int $id)
    {
        $row = $this->_Adapter->find(['id_category' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }
    
    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(EmailcategoryInterface $Emailcategory){
        
        $data = parent::getDataObject($Emailcategory);
        
        if (is_null($Emailcategory->id_category)) { 
$Emailcategory->id_category = $this->_Adapter->insert($data);
} else {
$this->_Adapter->update($data, ['id_category' => $Emailcategory->id_category]);
}
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($Emailcategory)
    {

        // if $id is a object and a UserEntity  
        if ($Emailcategory instanceof EmailcategoryInterface) {
            $Emailcategory = $Emailcategory->id_category;
        }

        return $this->_Adapter->delete(['id_category' => $Emailcategory]);
    }
    
        /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): EmailcategoryEntity
    {
        $EmailcategoryEntity = new EmailcategoryEntity($row);
        return $EmailcategoryEntity;
    }
 
 
}