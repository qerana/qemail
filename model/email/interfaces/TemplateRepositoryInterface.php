<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\interfaces;
use  Qerapp\qemail\model\email\interfaces\TemplateInterface;
/*
  |*****************************************************************************
  | [{model_name}]RepositoryInterface
  |*****************************************************************************
  |
  | Repository INTERFACE [{model_name}]
  | @author qDevTools,
  | @date 2020-02-27 06:39:30,
  |*****************************************************************************
 */

interface TemplateRepositoryInterface 
{
    
    public function findAll(array $conditions = [],array $options = []);
    
     public function findById_qemail_tpl(int  $id_qemail_tpl,array $options = []);
 public function findByTpl_name(string $tpl_name,array $options = []);
 public function findByTpl_type(string $tpl_type,array $options = []);
 public function findByTpl_content(string $tpl_content,array $options = []);
 public function findByTpl_active(int  $tpl_active,array $options = []);
 public function findByTpl_settings(string $tpl_settings,array $options = []);

    
    public function store(TemplateInterface $User);
    
    public function remove($id);
 
 
}