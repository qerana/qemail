<?php

/*
 * Copyright (C) 2019/20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\entity;

use \Ada\EntityManager,
    Qerapp\qemail\model\email\interfaces\EmailInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for Email
  |*****************************************************************************
  |
  | Entity Email
  | @author qDevTools,
  | @date 2020-02-16 16:30:05,
  |*****************************************************************************
 */

class EmailEntity extends EntityManager implements EmailInterface, \JsonSerializable {

    protected

    //ATRIBUTES        

    /** @var int(11), $id_category  */
            $_id_category,
            /** @var int(11), $type  */
            $_type,
            /** @var int(11), $id_email  */
            $_id_email,
            /** @var int(11), $id_mail_account  */
            $_id_account,
            /** @var text(), $destination  */
            $_destination,
            /** @var text(), $bcc  */
            $_bcc,
            /** @var varchar(45), $subject  */
            $_subject,
            /** @var text(), $body  */
            $_body,
            /** @var timestamp(), $created_at  */
            $_created_at,
            /** @var varchar(200), $created_by  */
            $_created_by,
            /** @var int(2), $sw_status  */
            $_sw_status;
    public
            $log_type = 'qemail',
            /**
             * @object Account email sender
             */
            $Account,
            $Category,
            /**
             * @array email logs
             */
            $logs = [],
            /**
             * @array attachments
             */
            $attachments = [];

    //RELATED

    public function __construct(array $data = []) {
        $this->populate($data);
        $this->attachments = \Ada\EntityRelation::hasMany($this, 'mailattachment', 'id_email');
        $this->setAccount();
        //RELATED-LOAD
    }

    public function setAccount() {
        if (!empty($this->_id_account)) {
            $this->Account = \Ada\EntityRelation::hasOne($this, 'account', 'id_account');
        }
    }

    public function setCategory() {

        if ($this->_id_category != '0') {
            $this->Category = \Ada\EntityRelation::hasOne($this, 'emailcategory', 'id_category');
        }
    }

    /**
     * Send this email
     */
    public function send() {

        $Qemail = new \Qerapp\qemail\model\Qemail();
        $Qemail->setAgent(new \Qerapp\qemail\model\agents\PhpMailerAgent($this->Account));
        $Qemail->sendEmail($this);
    }

    /**
     * get logs
     */
    public function setLogs() {
        $this->log_type = $this->log_type . '_' . $this->_id_email;
        $this->logs = \Ada\EntityRelation::hasMany($this, 'log', 'log_type', 'findAllByType');
    }

//SETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_category
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_category($id_category = 0): void {
        
        $this->_id_category = filter_var($id_category, FILTER_SANITIZE_STRING);
        if ($this->_id_category != '0') {
            $this->setCategory();
            $this->_id_account = $this->Category->id_account;
        }
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for type
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_type($type = 0): void {
        $this->_type = filter_var($type, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_email
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_email($id_email = 0): void {
        $this->_id_email = filter_var($id_email, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_mail_account
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_account($id_mail_account = 0): void {

        $this->_id_account = filter_var($id_mail_account, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for destination
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_destination($destination = ""): void {
        $this->_destination = filter_var($destination, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for bcc
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_bcc($bcc = ""): void {
        $this->_bcc = filter_var($bcc, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for subject
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_subject($subject = ""): void {
        $this->_subject = filter_var($subject, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for body
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_body($body = ""): void {
        $this->_body = $body;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for created_at
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_created_at($created_at = ""): void {
        $this->_created_at = filter_var($created_at, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for created_by
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_created_by($created_by = ""): void {
        $this->_created_by = filter_var($created_by, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for sw_status
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_sw_status($sw_status = 0): void {
        $this->_sw_status = filter_var($sw_status, FILTER_SANITIZE_STRING);
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_category
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_category() {
        return $this->_id_category;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for type
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_type() {
        return $this->_type;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_email
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_email() {
        return $this->_id_email;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_mail_account
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_account() {
        return $this->_id_account;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for destination
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_destination() {
        return filter_var($this->_destination, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for bcc
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_bcc() {
        return filter_var($this->_bcc, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for subject
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_subject() {
        return filter_var($this->_subject, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for body
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_body() {
        return $this->_body;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for created_at
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_created_at() {
        return \helpers\Date::toString($this->_created_at, 'd-m-Y H:i:s');
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for created_by
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_created_by() {
        return filter_var($this->_created_by, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for sw_status
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_sw_status() {
        return $this->_sw_status;
    }

}
