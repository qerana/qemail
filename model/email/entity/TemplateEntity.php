<?php

/*
 * Copyright (C) 2019/20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\entity;

use \Ada\EntityManager,
    Qerapp\qemail\model\email\interfaces\TemplateInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for Template
  |*****************************************************************************
  |
  | Entity Template
  | @author qDevTools,
  | @date 2020-02-27 06:39:30,
  |*****************************************************************************
 */

class TemplateEntity extends EntityManager implements TemplateInterface, \JsonSerializable
{

    protected

    //ATRIBUTES        

    /** @var int(11), $id_qemail_tpl  */
            $_id_qemail_tpl,
            /** @var varchar(100), $tpl_name  */
            $_tpl_name,
            /** @var varchar(100), $tpl_type  */
            $_tpl_type,
            /** @var text(), $tpl_content  */
            $_tpl_content,
            /** @var int(11), $tpl_active  */
            $_tpl_active,
            /** @var text(), $tpl_settings  */
            $_tpl_settings;

    //RELATED

    public function __construct(array $data = [])
    {
        $this->populate($data);

        //RELATED-LOAD
    }

//SETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_qemail_tpl
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_qemail_tpl($id_qemail_tpl = 0): void
    {
        if (!empty($id_qemail_tpl)) {
            try {
                $this->_id_qemail_tpl = filter_var($id_qemail_tpl,FILTER_SANITIZE_NUMBER_INT);
            } catch (\Exception $ex) {
                \QException\Exceptions::ShowException("Template.Error.Email", $ex);
            }
        }
    }

/**
     * ------------------------------------------------------------------------- 
     * Setter for tpl_name
     * ------------------------------------------------------------------------- 
     * @param string 
     */

    public function set_tpl_name($tpl_name = ""): void
    {
        $this->_tpl_name = filter_var($tpl_name, FILTER_SANITIZE_STRING);
    }

/**
     * ------------------------------------------------------------------------- 
     * Setter for tpl_type
     * ------------------------------------------------------------------------- 
     * @param string 
     */

    public function set_tpl_type($tpl_type = ""): void
    {
        $this->_tpl_type = filter_var($tpl_type, FILTER_SANITIZE_STRING);
    }

/**
     * ------------------------------------------------------------------------- 
     * Setter for tpl_content
     * ------------------------------------------------------------------------- 
     * @param string 
     */

    public function set_tpl_content($tpl_content = ""): void
    {
        $this->_tpl_content = filter_var($tpl_content, FILTER_SANITIZE_STRING);
    }

/**
     * ------------------------------------------------------------------------- 
     * Setter for tpl_active
     * ------------------------------------------------------------------------- 
     * @param int  
     */

    public function set_tpl_active($tpl_active = 0): void
    {
        $this->_tpl_active = filter_var($tpl_active, FILTER_SANITIZE_STRING);
    }

/**
     * ------------------------------------------------------------------------- 
     * Setter for tpl_settings
     * ------------------------------------------------------------------------- 
     * @param string 
     */

    public function set_tpl_settings($tpl_settings = ""): void
    {
        $this->_tpl_settings = filter_var($tpl_settings, FILTER_SANITIZE_STRING);
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_qemail_tpl
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_qemail_tpl()
    {
        return $this->_id_qemail_tpl;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for tpl_name
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_tpl_name()
    {
        return filter_var($this->_tpl_name, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for tpl_type
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_tpl_type()
    {
        return filter_var($this->_tpl_type, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for tpl_content
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_tpl_content()
    {
        return filter_var($this->_tpl_content, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for tpl_active
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_tpl_active()
    {
        return $this->_tpl_active;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for tpl_settings
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_tpl_settings()
    {
        return filter_var($this->_tpl_settings, FILTER_SANITIZE_STRING);
    }

}
