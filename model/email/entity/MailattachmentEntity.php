<?php

/*
 * Copyright (C) 2019/20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\entity;

use \Ada\EntityManager,
    Qerapp\qemail\model\email\interfaces\MailattachmentInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for Mailattachment
  |*****************************************************************************
  |
  | Entity Mailattachment
  | @author qDevTools,
  | @date 2020-02-18 06:31:59,
  |*****************************************************************************
 */

class MailattachmentEntity extends EntityManager implements MailattachmentInterface, \JsonSerializable
{

    protected

    //ATRIBUTES        

    /** @var int(11), $id_email_attachment  */
            $_id_email_attachment,
            /** @var int(11), $id_email  */
            $_id_email,
            /** @var varchar(255), $path_attachment  */
            $_path_attachment;

    //RELATED

    public function __construct(array $data = [])
    {
        $this->populate($data);

        //RELATED-LOAD
    }

//SETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_email_attachment
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_email_attachment($id_email_attachment = 0): void
    {
        
        $this->_id_email_attachment = filter_var($id_email_attachment,FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_email
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_email($id_email = 0): void
    {
       $this->_id_email = filter_var($id_email,FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for path_attachment
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_path_attachment($path_attachment = ""): void
    {
        $this->_path_attachment = filter_var($path_attachment, FILTER_SANITIZE_STRING);
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_email_attachment
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_email_attachment()
    {
        return $this->_id_email_attachment;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_email
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_email()
    {
        return $this->_id_email;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for path_attachment
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_path_attachment()
    {
        return filter_var($this->_path_attachment, FILTER_SANITIZE_STRING);
    }

}
