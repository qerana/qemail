<?php

/*
 * Copyright (C) 2019/20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\entity;

use \Ada\EntityManager,
    Qerapp\qemail\model\email\interfaces\EmailcategoryInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for Emailcategory
  |*****************************************************************************
  |
  | Entity Emailcategory
  | @author qDevTools,
  | @date 2020-10-10 16:51:03,
  |*****************************************************************************
 */

class EmailcategoryEntity extends EntityManager implements EmailcategoryInterface, \JsonSerializable {

    public
            $Account;
    protected

    //ATRIBUTES        

    /** @var int(11), $id_category  */
            $_id_category,
            /** @var varchar(45), $category  */
            $_category,
            /** @var int(11), $id_account  */
            $_id_account;

    //RELATED

    public function __construct(array $data = []) {
        $this->populate($data);

        //RELATED-LOAD
    }

//SETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_category
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_category($id_category = 0): void {
        $this->_id_category = filter_var($id_category, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for category
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_category($category = ""): void {
        $this->_category = filter_var($category, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_account
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_account($id_account = 0): void {
        $this->_id_account = filter_var($id_account, FILTER_SANITIZE_STRING);
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_category
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_category() {
        return $this->_id_category;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for category
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_category() {
        return filter_var($this->_category, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_account
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_account() {
        return $this->_id_account;
    }

}
