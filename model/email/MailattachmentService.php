<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email;

use 
    Qerapp\qemail\model\email\entity\MailattachmentEntity,
    Qerapp\qemail\model\email\mapper\MailattachmentMapper,
    Qerapp\qemail\model\email\interfaces\MailattachmentMapperInterface,
    Qerapp\qemail\model\email\repository\MailattachmentRepository;

        //RELATED-MAPPERS
        
/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity MailattachmentService
  | @author qDevTools,
  | @date 2020-02-18 06:31:59,
  |*****************************************************************************
 */

class MailattachmentService
{
    
    public
            //RELATED-MAPPER-OBJECT
            $MailattachmentRepository;
            
    public function __construct(MailattachmentMapperInterface $Mapper = null)
    {
        
         //RELATED-MAPPER-OBJECT-NEW
        
        
        
        try{
             $MailattachmentMapper = new MailattachmentMapper;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.MailattachmentService',$ex);
        }
        
         $MapperRepository = (is_null($Mapper)) ? $MailattachmentMapper : $Mapper;
         $this->MailattachmentRepository = new MailattachmentRepository($MapperRepository);
        
    }
    /**
     * -------------------------------------------------------------------------
     * simple test
     * -------------------------------------------------------------------------
     */
    public function runTest()
    {


        echo '<h1>Test</h1>';

       
    }
    
    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false){
        
        $Collection = $this->MailattachmentRepository->findAll(); 
        if($json){
            echo json_encode($Collection);
        }else{
            return $Collection;
        }
        
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id,$json = false){
        
        $Entity = $this->MailattachmentRepository->findById($id);
        if($json){
            echo json_encode($Entity);
        }else{
            return $Entity;
        }
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Save 
     * -------------------------------------------------------------------------
     */
   
    public function save(array $data = []){
        
        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;
        
        $Mailattachment = new MailattachmentEntity($data_to_save);
        $this->MailattachmentRepository->store($Mailattachment);
        
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function delete(int $id){
        return $this->MailattachmentRepository->remove($id);
    }
    
 
}