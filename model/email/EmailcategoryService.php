<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email;

use 
    Qerapp\qemail\model\email\entity\EmailcategoryEntity,
    Qerapp\qemail\model\email\mapper\EmailcategoryMapper,
    Qerapp\qemail\model\email\interfaces\EmailcategoryMapperInterface,
    Qerapp\qemail\model\email\repository\EmailcategoryRepository;

        //RELATED-MAPPERS
        
/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity EmailcategoryService
  | @author qDevTools,
  | @date 2020-10-10 16:51:03,
  |*****************************************************************************
 */

class EmailcategoryService
{
    
    public
            //RELATED-MAPPER-OBJECT
            $EmailcategoryRepository,
            /** @object entity Emailcategory*/
            $Emailcategory;
            
    public function __construct(EmailcategoryMapperInterface $Mapper = null)
    {
        
         //RELATED-MAPPER-OBJECT-NEW
        
        
        
        try{
             $EmailcategoryMapper = new EmailcategoryMapper;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.EmailcategoryService',$ex);
        }
        
         $MapperRepository = (is_null($Mapper)) ? $EmailcategoryMapper : $Mapper;
         $this->EmailcategoryRepository = new EmailcategoryRepository($MapperRepository);
        
    }
    /**
     * -------------------------------------------------------------------------
     * simple test
     * -------------------------------------------------------------------------
     */
    public function runTest()
    {


        echo '<h1>Test</h1>';

       
    }
    
    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false){
        
        $Collection = $this->EmailcategoryRepository->findAll(); 
        if($json){
            echo json_encode($Collection);
        }else{
            return $Collection;
        }
        
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id,$json = false){
        
        $this->Emailcategory = $this->EmailcategoryRepository->findById($id);
        if($this->Emailcategory){
            if($json){
            echo json_encode($this->Emailcategory);
        }else{
            return $this->Emailcategory;
        }
        }else{
            \QException\Exceptions::showHttpStatus(404, 'Emailcategory '.$id.' NOT FOUND!!');
        }
        
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Save  array data
     * -------------------------------------------------------------------------
     */
   
    public function save(array $data = []){
        
        $this->createEmailcategory($data);
        $this->storeEmailcategory($this->Emailcategory);
        
    }
    
    
    /**
     * Activate account on category
     * @param int $id_category
     * @param int $id_account
     */
    public function activateTask(int $id_category , int $id_account){
        
        $this->getById($id_category);
        $this->Emailcategory->id_account = $id_account;
        $this->storeEmailcategory($this->Emailcategory);
        
    }

     /**
     * -------------------------------------------------------------------------
     * Save  Object
     * -------------------------------------------------------------------------
     */
    public function storeEmailcategory($Emailcategory){
        
        $this->EmailcategoryRepository->store($Emailcategory);
        
        
    }
    
    
      /**
     *  Create empleado entity
     * @param array $data
     */
    public function createEmailcategory(array $data = []) {

        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;
        $this->Emailcategory = new EmailcategoryEntity($data_to_save);
    }
    
    
     /**
     * -------------------------------------------------------------------------
     * Delete 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function delete(int $id){
        return $this->EmailcategoryRepository->remove($id);
    }
    
 
}
