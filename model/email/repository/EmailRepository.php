<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\repository;

use Qerapp\qemail\model\email\interfaces\EmailMapperInterface,
    Qerapp\qemail\model\email\interfaces\EmailRepositoryInterface,
    Qerapp\qemail\model\email\interfaces\EmailInterface;

/*
  |*****************************************************************************
  | EmailRepositoryRepository
  |*****************************************************************************
  |
  | Repository EmailRepository
  | @author qDevTools,
  | @date 2020-02-16 16:30:05,
  |*****************************************************************************
 */

class EmailRepository implements EmailRepositoryInterface
{

    private
            $_EmailMapper;

    public function __construct(EmailMapperInterface $Mapper)
    {

        $this->_EmailMapper = $Mapper;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all EmailRepository
     * -------------------------------------------------------------------------
     * @return EmailRepositoryEntity collection
     */
    public function findById(int $id)
    {
        return $this->_EmailMapper->findOne(['id_email' => $id]);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all EmailRepository
     * -------------------------------------------------------------------------
     * @return EmailRepositoryEntity collection
     */
    public function findAll(array $conditions = [], array $options = [])
    {
        return $this->_EmailMapper->findAll($conditions, $options);
    }

    /**
     *  find by type and email not send
     * @param string $email
     * @return type
     */
    public function findbyTypeAnEmail(string $email,int $type)
    {

        return $this->_EmailMapper->findOne(
                        [
                            'destination' => $email,
                            'type' => $type,
                            'sw_status' => '0'
                        ],[
                            'orderby'=> 'created_at DESC '
                        ]
        );
    }


    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_email
     * ------------------------------------------------------------------------- 
     * @param id_email 
     */
    public function findById_email(int $id_email, array $options = [])
    {
        return $this->_EmailMapper->findAll(['id_email' => $id_email], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_mail_account
     * ------------------------------------------------------------------------- 
     * @param id_mail_account 
     */
    public function findById_account(int $id_account, array $options = [])
    {
        return $this->_EmailMapper->findAll(['id_account' => $id_account], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  destination
     * ------------------------------------------------------------------------- 
     * @param destination 
     */
    public function findByDestination(string $destination, array $options = [])
    {
        return $this->_EmailMapper->findAll(['destination' => $destination], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  bcc
     * ------------------------------------------------------------------------- 
     * @param bcc 
     */
    public function findByBcc(string $bcc, array $options = [])
    {
        return $this->_EmailMapper->findAll(['bcc' => $bcc], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  subject
     * ------------------------------------------------------------------------- 
     * @param subject 
     */
    public function findBySubject(string $subject, array $options = [])
    {
        return $this->_EmailMapper->findAll(['subject' => $subject], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  body
     * ------------------------------------------------------------------------- 
     * @param body 
     */
    public function findByBody(string $body, array $options = [])
    {
        return $this->_EmailMapper->findAll(['body' => $body], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  created_at
     * ------------------------------------------------------------------------- 
     * @param created_at 
     */
    public function findByCreated_at(string $created_at, array $options = [])
    {
        return $this->_EmailMapper->findAll(['created_at' => $created_at], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  created_by
     * ------------------------------------------------------------------------- 
     * @param created_by 
     */
    public function findByCreated_by(string $created_by, array $options = [])
    {
        return $this->_EmailMapper->findAll(['created_by' => $created_by], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  sw_status
     * ------------------------------------------------------------------------- 
     * @param sw_status 
     */
    public function findBySw_status(int $sw_status, array $options = [])
    {
        return $this->_EmailMapper->findAll(['sw_status' => $sw_status], $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Save EmailRepository
     * -------------------------------------------------------------------------
     * @object $EmailRepositoryEntity
     * @return type
     */
    public function store(EmailInterface $EmailRepositoryEntity)
    {
        return $this->_EmailMapper->save($EmailRepositoryEntity);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete EmailRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_EmailMapper->delete($id);
    }

}
