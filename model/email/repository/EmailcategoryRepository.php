<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\repository;
use Qerapp\qemail\model\email\interfaces\EmailcategoryMapperInterface,
Qerapp\qemail\model\email\interfaces\EmailcategoryRepositoryInterface,
Qerapp\qemail\model\email\interfaces\EmailcategoryInterface;
/*
  |*****************************************************************************
  | EmailcategoryRepositoryRepository
  |*****************************************************************************
  |
  | Repository EmailcategoryRepository
  | @author qDevTools,
  | @date 2020-10-10 16:51:03,
  |*****************************************************************************
 */

class EmailcategoryRepository implements EmailcategoryRepositoryInterface
{
    
    private
            $_EmailcategoryMapper;
            
    public function __construct(EmailcategoryMapperInterface $Mapper)
    {
        
         $this->_EmailcategoryMapper = $Mapper;
        
    }
          /**
     * -------------------------------------------------------------------------
     * Get One
     * -------------------------------------------------------------------------
     * @return EmailcategoryRepository
     */
    
     public function find(array $condition = [],array $options = []) {
        return $this->_EmailcategoryMapper->findOne($condition,$options);
    }
    
    
      /**
     * -------------------------------------------------------------------------
     * Get all EmailcategoryRepository
     * -------------------------------------------------------------------------
     * @return EmailcategoryRepositoryEntity collection
     */
    
    public function findById(int $id)
    {
        return $this->_EmailcategoryMapper->findOne(['id_category'=>$id]);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Get all EmailcategoryRepository
     * -------------------------------------------------------------------------
     * @return EmailcategoryRepositoryEntity collection
     */
    
    public function findAll(array $conditions = [],array $options = [])
    {
        return $this->_EmailcategoryMapper->findAll($conditions,$options);
    }
    
    
    /** 
* ------------------------------------------------------------------------- 
* Fin by  id_category
* ------------------------------------------------------------------------- 
* @param id_category 
*/ 
  public function findById_category(int  $id_category,array $options = [])
{ 
return $this->_EmailcategoryMapper->findAll(['id_category'=> $id_category],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  category
* ------------------------------------------------------------------------- 
* @param category 
*/ 
  public function findByCategory(string $category,array $options = [])
{ 
return $this->_EmailcategoryMapper->findAll(['category'=> $category],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  id_account
* ------------------------------------------------------------------------- 
* @param id_account 
*/ 
  public function findById_account(int  $id_account,array $options = [])
{ 
return $this->_EmailcategoryMapper->findAll(['id_account'=> $id_account],$options);
}
    
   
    
     /**
     * -------------------------------------------------------------------------
     * Save EmailcategoryRepository
     * -------------------------------------------------------------------------
     * @object $EmailcategoryRepositoryEntity
     * @return type
     */
    public function store(EmailcategoryInterface $EmailcategoryRepositoryEntity)
    {
        return $this->_EmailcategoryMapper->save($EmailcategoryRepositoryEntity);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete EmailcategoryRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_EmailcategoryMapper->delete($id);
    }
 
}
