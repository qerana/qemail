<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\repository;
use Qerapp\qemail\model\email\interfaces\MailattachmentMapperInterface,
Qerapp\qemail\model\email\interfaces\MailattachmentRepositoryInterface,
Qerapp\qemail\model\email\interfaces\MailattachmentInterface;
/*
  |*****************************************************************************
  | MailattachmentRepositoryRepository
  |*****************************************************************************
  |
  | Repository MailattachmentRepository
  | @author qDevTools,
  | @date 2020-02-18 06:31:59,
  |*****************************************************************************
 */

class MailattachmentRepository implements MailattachmentRepositoryInterface
{
    
    private
            $_MailattachmentMapper;
            
    public function __construct(MailattachmentMapperInterface $Mapper)
    {
        
         $this->_MailattachmentMapper = $Mapper;
        
    }
    
    
      /**
     * -------------------------------------------------------------------------
     * Get all MailattachmentRepository
     * -------------------------------------------------------------------------
     * @return MailattachmentRepositoryEntity collection
     */
    
    public function findById(int $id)
    {
        return $this->_MailattachmentMapper->findOne(['id_email_attachment'=>$id]);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Get all MailattachmentRepository
     * -------------------------------------------------------------------------
     * @return MailattachmentRepositoryEntity collection
     */
    
    public function findAll(array $conditions = [],array $options = [])
    {
        return $this->_MailattachmentMapper->findAll($conditions,$options);
    }
    
    
    /** 
* ------------------------------------------------------------------------- 
* Fin by  id_email_attachment
* ------------------------------------------------------------------------- 
* @param id_email_attachment 
*/ 
  public function findById_email_attachment(int  $id_email_attachment,array $options = [])
{ 
return $this->_MailattachmentMapper->findAll(['id_email_attachment'=> $id_email_attachment],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  id_email
* ------------------------------------------------------------------------- 
* @param id_email 
*/ 
  public function findById_email(int  $id_email,array $options = [])
{ 
return $this->_MailattachmentMapper->findAll(['id_email'=> $id_email],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  path_attachment
* ------------------------------------------------------------------------- 
* @param path_attachment 
*/ 
  public function findByPath_attachment(string $path_attachment,array $options = [])
{ 
return $this->_MailattachmentMapper->findAll(['path_attachment'=> $path_attachment],$options);
}
    
   
    
     /**
     * -------------------------------------------------------------------------
     * Save MailattachmentRepository
     * -------------------------------------------------------------------------
     * @object $MailattachmentRepositoryEntity
     * @return type
     */
    public function store(MailattachmentInterface $MailattachmentRepositoryEntity)
    {
        return $this->_MailattachmentMapper->save($MailattachmentRepositoryEntity);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete MailattachmentRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_MailattachmentMapper->delete($id);
    }
 
}