<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email\repository;
use Qerapp\qemail\model\email\interfaces\TemplateMapperInterface,
Qerapp\qemail\model\email\interfaces\TemplateRepositoryInterface,
Qerapp\qemail\model\email\interfaces\TemplateInterface;
/*
  |*****************************************************************************
  | TemplateRepositoryRepository
  |*****************************************************************************
  |
  | Repository TemplateRepository
  | @author qDevTools,
  | @date 2020-02-27 06:39:30,
  |*****************************************************************************
 */

class TemplateRepository implements TemplateRepositoryInterface
{
    
    private
            $_TemplateMapper;
            
    public function __construct(TemplateMapperInterface $Mapper)
    {
        
         $this->_TemplateMapper = $Mapper;
        
    }
    
    
      /**
     * -------------------------------------------------------------------------
     * Get all TemplateRepository
     * -------------------------------------------------------------------------
     * @return TemplateRepositoryEntity collection
     */
    
    public function findById(int $id)
    {
        return $this->_TemplateMapper->findOne(['id_qemail_tpl'=>$id]);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Get all TemplateRepository
     * -------------------------------------------------------------------------
     * @return TemplateRepositoryEntity collection
     */
    
    public function findAll(array $conditions = [],array $options = [])
    {
        return $this->_TemplateMapper->findAll($conditions,$options);
    }
    
    
    /** 
* ------------------------------------------------------------------------- 
* Fin by  id_qemail_tpl
* ------------------------------------------------------------------------- 
* @param id_qemail_tpl 
*/ 
  public function findById_qemail_tpl(int  $id_qemail_tpl,array $options = [])
{ 
return $this->_TemplateMapper->findAll(['id_qemail_tpl'=> $id_qemail_tpl],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  tpl_name
* ------------------------------------------------------------------------- 
* @param tpl_name 
*/ 
  public function findByTpl_name(string $tpl_name,array $options = [])
{ 
return $this->_TemplateMapper->findAll(['tpl_name'=> $tpl_name],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  tpl_type
* ------------------------------------------------------------------------- 
* @param tpl_type 
*/ 
  public function findByTpl_type(string $tpl_type,array $options = [])
{ 
return $this->_TemplateMapper->findAll(['tpl_type'=> $tpl_type],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  tpl_content
* ------------------------------------------------------------------------- 
* @param tpl_content 
*/ 
  public function findByTpl_content(string $tpl_content,array $options = [])
{ 
return $this->_TemplateMapper->findAll(['tpl_content'=> $tpl_content],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  tpl_active
* ------------------------------------------------------------------------- 
* @param tpl_active 
*/ 
  public function findByTpl_active(int  $tpl_active,array $options = [])
{ 
return $this->_TemplateMapper->findAll(['tpl_active'=> $tpl_active],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  tpl_settings
* ------------------------------------------------------------------------- 
* @param tpl_settings 
*/ 
  public function findByTpl_settings(string $tpl_settings,array $options = [])
{ 
return $this->_TemplateMapper->findAll(['tpl_settings'=> $tpl_settings],$options);
}
    
   
    
     /**
     * -------------------------------------------------------------------------
     * Save TemplateRepository
     * -------------------------------------------------------------------------
     * @object $TemplateRepositoryEntity
     * @return type
     */
    public function store(TemplateInterface $TemplateRepositoryEntity)
    {
        return $this->_TemplateMapper->save($TemplateRepositoryEntity);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete TemplateRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_TemplateMapper->delete($id);
    }
 
}