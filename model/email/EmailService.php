<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qemail\model\email;

use Qerapp\qemail\model\email\entity\EmailEntity,
    Qerapp\qemail\model\email\mapper\EmailMapper,
    Qerapp\qemail\model\email\interfaces\EmailMapperInterface,
    Qerapp\qemail\model\email\repository\EmailRepository,
    Qerapp\qemail\model\account\AccountService;

//RELATED-MAPPERS

/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity EmailService
  | @author qDevTools,
  | @date 2020-02-16 16:30:05,
  |*****************************************************************************
 */

class EmailService {

    public
            
            $Email,
    //RELATED-MAPPER-OBJECT
            $EmailRepository;

    public function __construct(EmailMapperInterface $Mapper = null) {

        //RELATED-MAPPER-OBJECT-NEW



        try {
            $EmailMapper = new EmailMapper;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.EmailService', $ex);
        }

        $MapperRepository = (is_null($Mapper)) ? $EmailMapper : $Mapper;
        $this->EmailRepository = new EmailRepository($MapperRepository);
    }

    /**
     * Get all account
     * @return type
     */
    public function getAccounts() {
        $AccountService = new AccountService();
        return $AccountService->getAll();
    }

    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false) {

        $Collection = $this->EmailRepository->findAll();


        if ($json) {
            echo json_encode($Collection);
            //print_r($Collection);
        } else {
            return $Collection;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false) {

        $Entity = $this->EmailRepository->findById($id);
        $this->Email = $Entity;
        if ($json) {
            echo json_encode($Entity);
        } else {
            return $Entity;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save 
     * -------------------------------------------------------------------------
     */
    public function save(array $data = []) {

        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;

        $this->Email = new EmailEntity($data_to_save);
        $this->EmailRepository->store($this->Email);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function delete(int $id) {
        return $this->EmailRepository->remove($id);
    }

}
