// load all data
function loadEmail() {
    getDataJson('/qemail/Email/getAllInJson', 'fill()');
}

// fill index table
function fill() {

    var list_Email = '';

    $.each(data_response, function (i, Email) {
        list_Email += '<tr class="small">';
        list_Email += '<td><a href="/qemail/Email/detail/' + Email.id_email + '" class="btn btn-outline-primary btn-sm" title="Detalle Email"><i class="fa fa-tasks"></i></a>';

        list_Email += '<td>' + Email.Account.address+ '</td>';
        list_Email += '<td>' + Email.destination + '</td>';
        list_Email += '<td>' + Email.subject + '</td>';
        list_Email += '<td>' + Email.sw_status + '</td>';
        list_Email += '<td>' + Email.created_at + '</td>';

        list_Email += '</tr>';

    });
    $('#list_Email').html(list_Email);

}

// load data
function loadDataEmail() {

    var id_email = $('#data_Email').attr("data-Email");
    getDataJson('/qemail/Email/getOneInJson/' + id_email, 'fillDataEmail()');

}


// fill data detail
function fillDataEmail() {

    var data_json = ' ';
    var detail_title = '';

    $.each(data_response, function (i, Email) {


        data_json += ' <div class="row m-1">';
        data_json += '    <div class="col-sm-3 bg-gray-100 p-2">';
        data_json += '        <b>' + i + '</b>';
        data_json += '    </div>';
        data_json += '    <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_' + i + '">';
        data_json += '        ' + Email + '';
        data_json += '    </div>';
        data_json += '</div>';

    });


    $('#detail_title').html(detail_title);
    $('#data_Email').html(data_json);

}