// load all data
function loadEmailCategory() {
    getDataJson('/qemail/EmailCategory/getAllInJson', 'fill()');
}

// fill index table
function fill() {

    var list_EmailCategory = '';

    $.each(data_response, function (i, EmailCategory) {
            list_EmailCategory += '<tr class="small">';
            list_EmailCategory += '<td><a href="/qemail/EmailCategory/detail/'+EmailCategory.id_category+'" class="btn btn-outline-primary btn-sm" title="Detalle EmailCategory"><i class="fa fa-tasks"></i></a>';

            list_EmailCategory +='<td>'+EmailCategory.id_category+'</td>'; 
list_EmailCategory +='<td>'+EmailCategory.category+'</td>'; 
list_EmailCategory +='<td>'+EmailCategory.id_account+'</td>'; 

            list_EmailCategory += '</tr>';

    });
    $('#list_EmailCategory').html(list_EmailCategory);

}

// load data
function loadDataEmailCategory() {

    var id_category = $('#data_EmailCategory').attr("data-EmailCategory");
    getDataJson('/qemail/EmailCategory/getOneInJson/' + id_category, 'fillDataEmailCategory()');

}


// fill data detail
function fillDataEmailCategory() {

    var data_json = ' ';
    var detail_title = '';
    
    $.each(data_response, function (i,EmailCategory) {
        
        
        data_json += ' <div class="row m-1">';
        data_json += '    <div class="col-sm-3 bg-gray-100 p-2">';
        data_json += '        <b>'+i+'</b>';
        data_json += '    </div>';
        data_json += '    <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_'+i+'">';
        data_json += '        '+EmailCategory+'';
        data_json += '    </div>';
        data_json += '</div>';

    });
    
    
    $('#detail_title').html(detail_title);
    $('#data_EmailCategory').html(data_json);

}