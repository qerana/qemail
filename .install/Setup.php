<?php

/*
 * This file is part of QAccess
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Setup {

    public function run() {

        $this->copyXmlStructure();
        $this->copyJsScripts();
        $output = '============================================================' . "\n";
        $output .= " Qemail successfull installed \n";
        $output .= '-----------------------------------------------------------' . "\n";
        $output .= " This qerapp is not visible on nav, please edit xml module and set visible to 1 \n";
        $output .= " If ACL is actived run #./cliqer acl:add --user=your_user--url=/qemail/account/,/qemail/email/ \n";
        $output .= '============================================================' . "\n";

        echo $output;
    }

    /**
     *  Create xml structure to _DATA_/xml/qaccess
     */
    private function copyXmlStructure() {


        $dir_target = realpath(__DATA__) . '/xml/qemail/';
        $dir_source = realpath(__QERAPPSFOLDER__ . 'qemail/.install/_data/xml/');
        shell_exec('cp -r ' . $dir_source . ' ' . $dir_target);
    }

    private function copyJsScripts() {

        $dir_target = realpath(__DOCUMENTROOT__) . '/src/js/app/qemail/';
        // create target folder
        mkdir($dir_target, 0777, true);

        $dir_source = realpath(__QERAPPSFOLDER__ . 'qemail/.install/_data/');
        shell_exec('cp -r ' . $dir_source . '/account.js ' . $dir_target);
        shell_exec('cp -r ' . $dir_source . '/email.js ' . $dir_target);
        shell_exec('cp -r ' . $dir_source . '/template.js ' . $dir_target);
        shell_exec('cp -r ' . $dir_source . '/emailcategory.js ' . $dir_target);
    }

}
